"""Synthesis results.

Temperature is the same (25C)
"""

from enum import Enum
from collections import namedtuple
from pathlib import Path
import argparse
import itertools
import re

import numpy as np

WARNINGS = []

def nm(nanometres):
    return nanometres * 1e-9


def mhz(megahertzes):
    return megahertzes * 1e6


def us(microseconds):
    return microseconds * 1e-6


def mw(miliwatts):
    return miliwatts * 1e-3


def v(volts):
    return float(volts)


class Ref:
    tech = nm(65)  # Reference technology
    volt = 1.0     # Reference voltage
    wl   = 16      # Reference word length


def energy(power, time):
    return power * time


def norm_energy(en, tech, volt, wl):
    num = np.prod([Ref.tech, Ref.volt**2, Ref.wl**2/3 + 2*Ref.wl/3])
    den = np.prod([    tech,     volt**2,     wl**2/3 +     2*wl/3])
    return en * num / den


def norm_power(power, volt, nexp, freq):
    den = np.prod([volt**2, 2**nexp, freq])
    return power / den * 1000 * 1e10


class Result:
    def __init__(self, nexp, wl, tech, volt, freq, power, arch_type,
                 time=None, note=''):
        self.nexp        = nexp
        self.wl          = wl
        self.tech        = tech
        self.volt        = volt
        self.freq        = freq
        self.power       = power
        if time:
            self.energy      = energy(power, time)
            self.energy_norm = norm_energy(self.energy, tech, volt, wl)
            self.time        = time
        else:
            self.energy      = -1
            self.energy_norm = -1
            self.time        = us(-1)
        self.arch_type   = arch_type
        self.note        = note

    def data(self):
        uJ      = self.energy * 1e6
        uJ_norm = self.energy_norm * 1e6
        data = [
            2**self.nexp,
            self.tech * 1e9,
            self.volt,
            self.wl,
            self.freq / 1e6,
            self.time * 1e6,
            self.power * 1e3,
            int(1000/uJ),
            int(1000/uJ_norm),
            self.arch_type,
            self.note,
        ]
        return data


def print_table(data, columns, data_formats=['{}'],
                col_sep=" | ", hline_sep=None, hline_col=0,
                csv_file=None):
    print_rows = [columns]
    for data_row in data:
        row_iter = itertools.zip_longest(data_row, data_formats,
                                         fillvalue=data_formats[-1])
        row = [df.format(x) for (x, df) in row_iter]
        print_rows.append(row)
    print_rows = np.array(print_rows, str)
    col_widths = [ np.max([len(s) for s in col]) for col in print_rows.T ]

    first_row = [s.center(w) for (s, w) in zip(print_rows[0], col_widths)]
    print(col_sep.join(first_row))
    prev = None
    for print_row in print_rows[1:]:
        row = [s.rjust(w) for (s, w) in zip(print_row, col_widths)]
        cur = row[hline_col]
        if hline_sep and (cur != prev):
            print(hline_sep.join([n*'-' for n in col_widths]))
        print(col_sep.join(row))
        prev = cur

    if csv_file:
        with open(csv_file, 'w') as wf:
            for row in print_rows:
                wf.write(",".join(row) + '\n')


def external_results():
    """Collect results from other papers.
    """
    ref_a = [
        Result(10, 16, nm(130), v(1.5), mhz(250), mw(60.4), "mem", 5160/mhz(250), "ref [a]"),
        Result(10, 16, nm(130), v(1.1), mhz(140), mw(29.8), "mem", 5160/mhz(140), "ref [a]"),
        Result(13, 16, nm(130), v(1.5), mhz(250), mw(68.7), "mem", 57396/mhz(250), "ref [a]"),
    ]

    ref_b = [
        Result(6, 16, nm(180), v(1.8), mhz(50), mw(21.43), "pipe", us(0.01*64), "ref [b]"),
    ]

    ref_c = [
        Result(10, 16, nm(65), v(1.1), mhz(50), mw(17.6), "pipe SDF", 1024/mhz(50)+us(1.03), "ref [c]"),
    ]

    ref_d = [
        Result(7, 16, nm(180), v(1.8), mhz(25), mw(9.23), "pipe", 64/mhz(25), "ref [d]"),
    ]

    ref_e = [
        Result(10, 16, nm(65), v(1.2), mhz(500), mw(85),  "mem", us(10.288), "ref [e]_1"),
        Result(10, 16, nm(65), v(1.2), mhz(500), mw(170), "mem", us(2.572),  "ref [e]_2"),
        Result(11, 16, nm(65), v(1.2), mhz(500), mw(85),  "mem", us(22.576), "ref [e]_1"),
        Result(11, 16, nm(65), v(1.2), mhz(500), mw(170), "mem", us(11.312), "ref [e]_2"),
    ]

    ref_f = [
        Result(6, 16, nm(180), v(1.8), mhz(20), mw(6.45), "dist mem", 3/mhz(20), "ref [f]"),
    ]

    ref_g = [
        Result(10, 16, nm(90), v(1), mhz(160), mw(29), "mem", (1024/2.66+28)/mhz(160), "ref [g]"),
        Result(11, 16, nm(90), v(1), mhz(160), mw(29), "mem", (2048/2.66+28)/mhz(160), "ref [g]"),
        Result(12, 16, nm(90), v(1), mhz(160), mw(29), "mem", (4096/2.66+28)/mhz(160), "ref [g]"),
        Result(13, 16, nm(90), v(1), mhz(160), mw(29), "mem", (8192/2+28)/mhz(160), "ref [g]"),
        Result(14, 16, nm(90), v(1), mhz(160), mw(29), "mem", (16384/2+28)/mhz(160), "ref [g]"),
    ]

    ref_h = [
        Result(10, 16, nm(55), v(0.9), mhz(180), mw(8.88), "pipe", us(1.46), "ref [h]"),
    ]

    ref_i = [
        Result(7, 14, nm(180), v(1.8), mhz(300), mw(382), "pipe", 128/8/mhz(300), "ref [i]"),
        Result(8, 14, nm(180), v(1.8), mhz(300), mw(466), "pipe", 256/8/mhz(300), "ref [i]"),
        Result(9, 14, nm(180), v(1.8), mhz(300), mw(507), "pipe", 512/8/mhz(300), "ref [i]"),
    ]

    ref_j = [
        Result(10, 20, nm(600), v(1.1), mhz(16), mw(9.4), "mem", us(330), "ref [j]"),
        Result(10, 20, nm(600), v(3.3), mhz(173), mw(845), "mem", us(30), "ref [j]"),
    ]

    res = ref_a + ref_b + ref_c + ref_d + ref_e + ref_f + ref_g + ref_h + ref_i + ref_j

    return res


def my_results():
    """Automatically collect results from reports folders.
    """
    reports = Path("reports")

    patterns = {
        'integer': re.compile("\d+"),
        'float': re.compile("\d+\.\d+"),
    }

    notes = {
        'cactiblocks': 'mb',
        'cacti': 'ms',
    }

    times = {
        6: us(0.844),
        7: us(2.124),
        8: us(4.172),
        9: us(10.316),
        10: us(20.556),
        11: us(49.228),
        12: us(98.380),
        13: us(229.452),
        14: us(458.828),
    }

    results = []

    for d in reports.iterdir():
        metadata = d.name.split('_')

        try:
            tech = int(patterns['integer'].search(metadata[0]).group())
            volt = float(patterns['float'].search(metadata[1]).group())
            freq = int(patterns['integer'].search(metadata[2]).group())
            note = notes[metadata[3]]

            with open(d.joinpath("timing.txt"), 'r') as rf:
                lines = rf.readlines()
                for line in lines:
                    if "slack (VIOLATED)" in line:
                        WARNINGS.append(("Warning: skipped `{}` due to " +
                                        "timing violation").format(d.name))
                        continue

            for nexp in range(6, 15):
                with open(d.joinpath("power_{}.txt".format(nexp)), 'r') as rf:
                    lines = rf.readlines()
                    s_power = lines[-2].split()[-2]
                power = float(patterns['float'].search(s_power).group())
                time = times[nexp] * mhz(250) / freq
                results.append(Result(nexp, 16, nm(tech), v(volt), mhz(freq),
                                      mw(power), 'mem', us(time), note))
        except (AttributeError):
            WARNINGS.append("Warning: unrecognized directory name: `{}`"
                            .format(d.name))

    return results


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', action='store_true', default=False,
                        help="Generate CSV file from the results.")
    args = parser.parse_args()

    cols = [
        "FFT", "L (nm)", "Vcc (V)", "wl", "f (MHz)", "t (us)", "pwr (mW)",
        "FFT/mJ", "FFT/mJ_n", "type", "note"
    ]

    formats = [
        "{}", "{:.0f}", "{:.2f}", "{}", "{:.0f}", "{:.2f}", "{:.2f}",
        "{}", "{}", "{}", "{:<9}"
    ]

    results = my_results() + external_results()
    data = [result.data() for result in results]

    sort_cols = [0, 8, 1, 4]
    data = sorted(data, key = lambda x: tuple([x[s] for s in sort_cols]))

    csv_file = "results.csv" if args.csv else None

    print_table(data, cols, formats,
                col_sep=" | ", hline_sep="-+-", hline_col=0,
                csv_file=csv_file)

    references = [
        "[a] Pitkänen, Takala (2010):\n  " + \
            "Low-Power Application-Specific Processor for FFT Computations",
        "[b] Tran, Kanagawa, Nguyen, Nakashima (2016):\n  " + \
            "ASIC Design of MUL-RED Radix-2 Pipeline FFT Circuit for 802.11ah System",
        "[c] Garrido, Andersson, Qureshi, Gustafsson (2016):\n  " + \
            "Multiplierless Unity-Gain SDF FFTs",
        "[d] Sarada, Vigneswaran, Selvakumar (2018):\n  " + \
            "Low-power and high-throughput 128-point feedforward FFT processor",
        "[e] Shami, Tajammul, Hemani (2018):\n  " + \
            "Configurable FFT Processor Using Dynamically Reconfigurable Resource Arrays",
        "[f] Arunachalam, Raj (2014):\n  " + \
            "Efficient VLSI implementation of FFT for orthogonal frequency division multiplexing applications",
        "[g] Huang, Chen (2016):\n  " + \
            "A High-Parallelism Memory-Based FFT Processor with High SQNR and Novel Addressing Scheme",
        "[h] Garrido, Huang, Chen (2018):\n  " + \
            "Feedforward FFT Hardware Architectures Based on Rotator Allocation",
        "[i] Tang, Liao, Chang (2012):\n  " + \
            "An Area- and Energy-Efficient Multimode FFT Processor for WPAN/WLAN/WMAN Systems",
        "[j] Bass (1999):\n  " + \
            "A Low-Power, High-Performance, 1024-Point FFT Processor",
        "[x] Kuo, Wen, Lin, Wu (2003):\n  " + \
            "VLSI Design of a Variable-Length FFT/IFFT Processor for OFDM-Based Communication Systems",
    ]

    print("\nReferences:")
    for ref in references:
        print(ref)

    notes = [
    ]

    if notes:
        print("\nNotes:")
        for note in notes:
            print(note)
        print('')

    for w in WARNINGS:
        print(w)
