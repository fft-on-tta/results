 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 18:07:16 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2150 (99.22%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.78%)    |
          |                                       |                  |
          |    Total number of registers          |     2167         |
          ------------------------------------------------------------



1
