Information: Propagating switching activity (high effort zero delay simulation). (PWR-6)
Warning: Invalid switching activity annotation on constant net(s) is being ignored. (PWR-421)
Warning: Design has unannotated sequential cell outputs. (PWR-415)
Warning: Design has unannotated black box outputs. (PWR-428)
 
****************************************
Report : power
        -analysis_effort high
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 18:08:02 2018
****************************************


Library(s) Used:

    uk65lscllmvbbh_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51/ram_64x51.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32/ram_32x32.db)


Operating Conditions: uk65lscllmvbbh_100c25_tc   Library: uk65lscllmvbbh_100c25_tc
Wire Load Model Mode: Inactive.


Global Operating Voltage = 1    
Power-specific unit information :
    Voltage Units = 1V
    Capacitance Units = 1.000000pf
    Time Units = 1ns
    Dynamic Power Units = 1mW    (derived from V,C,T units)
    Leakage Power Units = 1pW

Warning: Cannot report correlated power unless power prediction mode is set. (PWR-727)
Power Breakdown
---------------

                             Cell        Driven Net  Tot Dynamic      Cell
                             Internal    Switching   Power (mW)       Leakage
Cell                         Power (mW)  Power (mW)  (% Cell/Tot)     Power (pW)
--------------------------------------------------------------------------------
Netlist Power                   1.7968      0.9737   2.770e+00 (65%)  1.596e+09
Estimated Clock Tree Power   N/A         N/A          (N/A)           N/A
--------------------------------------------------------------------------------

                 Internal         Switching           Leakage            Total
Power Group      Power            Power               Power              Power   (   %    )  Attrs
--------------------------------------------------------------------------------------------------
io_pad             0.0000            0.0000            0.0000            0.0000  (   0.00%)
memory             0.6841        9.6937e-03        1.5959e+09            2.2897  (  52.44%)
black_box          0.0000            0.0000            0.0000            0.0000  (   0.00%)
clock_network  6.6002e-02        7.5815e-02        5.9794e+03            0.1418  (   3.25%)
register           0.4793        3.4699e-02        1.1688e+05            0.5141  (  11.77%)
sequential         0.0000            0.0000            0.0000            0.0000  (   0.00%)
combinational      0.5674            0.8534        1.5782e+05            1.4210  (  32.54%)
--------------------------------------------------------------------------------------------------
Total              1.7968 mW         0.9736 mW     1.5962e+09 pW         4.3667 mW
1
