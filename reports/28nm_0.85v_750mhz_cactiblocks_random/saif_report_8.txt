Warning: There are 104 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 12:18:31 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         270(65.06%)       0(0.00%)          145(34.94%)        415
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         238(37.19%)       0(0.00%)          402(62.81%)        640
--------------------------------------------------------------------------------
1
