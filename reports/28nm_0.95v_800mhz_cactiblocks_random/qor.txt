 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 01:51:24 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              17.00
  Critical Path Length:          1.18
  Critical Path Slack:           0.01
  Critical Path Clk Period:      1.25
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        213
  Hierarchical Port Count:      11246
  Leaf Cell Count:              18840
  Buf/Inv Cell Count:            3576
  Buf Cell Count:                 944
  Inv Cell Count:                2632
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16489
  Sequential Cell Count:         2351
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8635.673866
  Noncombinational Area:  6405.926319
  Buf/Inv Area:           1030.662416
  Total Buffer Area:           431.72
  Total Inverter Area:         598.94
  Macro/Black Box Area:  96186.932556
  Net Area:                  0.000000
  Net XLength        :      105953.70
  Net YLength        :      100637.32
  -----------------------------------
  Cell Area:            111228.532741
  Design Area:          111228.532741
  Net Length        :       206591.03


  Design Rules
  -----------------------------------
  Total Number of Nets:         20566
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  316.03
  Logic Optimization:                 31.22
  Mapping Optimization:              195.13
  -----------------------------------------
  Overall Compile Time:              732.38
  Overall Compile Wall Clock Time:   580.32

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
