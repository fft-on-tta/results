 
****************************************
Report : area
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 01:51:23 2018
****************************************

Library(s) Used:

    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CORE_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs/C28SOI_SC_8_CORE_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x51/ram_64x51.db)
    C28SOI_SC_8_COREPBP10_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP4_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_32x32/ram_32x32.db)

Number of ports:                        11475
Number of nets:                         30330
Number of cells:                        19460
Number of combinational cells:          16489
Number of sequential cells:              2739
Number of macros/black boxes:              19
Number of buf/inv:                       3576
Number of references:                      55

Combinational area:               8635.673866
Buf/Inv area:                     1030.662416
Noncombinational area:            6405.926319
Macro/Black Box area:            96186.932556
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                111228.532741
Total area:                 undefined

Hierarchical area distribution
------------------------------

                                  Global cell area              Local cell area
                                  --------------------  -------------------------------- 
Hierarchical cell                 Absolute     Percent  Combi-     Noncombi-  Black-
                                  Total        Total    national   national   boxes       Design
--------------------------------  -----------  -------  ---------  ---------  ----------  -----------------------------------------------------------------------------------
proc                              111228.5327    100.0    93.1328   141.6576      0.0000  proc
clk_gate_cnt_rd_reg                    1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0_4_0
clk_gate_en_blocks_reg_reg             1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_block_mem_output_sel_DATAW32_NBLOCKS9_1_2
clk_gate_tf_reg                        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16_0
core/decomp                           22.5216      0.0    22.5216     0.0000      0.0000  tta0_decompressor
core/fu_AG                           638.1120      0.6    33.0752   115.9808      0.0000  fu_ag_always_2_dataw32_busw32_nexpw4
core/fu_AG/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_5
core/fu_AG/clk_gate_o1tmp_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_3
core/fu_AG/clk_gate_o2reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_4
core/fu_AG/clk_gate_o2tmp_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_2
core/fu_AG/clk_gate_r1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_1
core/fu_AG/clk_gate_r1reg_reg_0_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_6
core/fu_AG/clk_gate_t1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0
core/fu_AG/fu_arch                   477.6320      0.4   221.5168   169.9456      0.0000  ag_arith_dataw32_nexpw4_idxw16_stagew3_mau_shift2
core/fu_AG/fu_arch/u0                 86.1696      0.1    35.2512    50.9184      0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_0
core/fu_CADD1                       1323.1168      1.2     0.5440     0.0000      0.0000  fu_cadd_always_1_dataw32_busw32
core/fu_CADD1/fu_arch               1322.5728      1.2     0.0000     0.0000      0.0000  cadd_full_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_arith    967.3408     0.9  650.5152  316.8256     0.0000  cadd_arith_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_reg    355.2320     0.3   58.8608   280.0512      0.0000  cadd_reg_dataw32
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_8
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_9
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_4
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_0
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_1
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_2      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_2
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_3      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_3
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_7
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_c_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_6
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_cnt_reg_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_1_11
core/fu_CMUL                        2943.6929      2.6    80.9472   198.2336      0.0000  fu_cmul_always_3_dataw32_busw32
core/fu_CMUL/clk_gate_o1reg_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_0
core/fu_CMUL/clk_gate_o1reg_reg_0_0      1.6320     0.0    0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_3
core/fu_CMUL/clk_gate_o1tmp_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_2
core/fu_CMUL/clk_gate_r1reg_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_1
core/fu_CMUL/fu_arch                2657.9841      2.4  2475.0913   181.2608      0.0000  cmul_pipe_1_dataw32_halfw16
core/fu_CMUL/fu_arch/clk_gate_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16
core/fu_DLY11                       1340.6336      1.2     0.6528   107.7120      0.0000  fu_rotreg_always_11_dataw32_busw32
core/fu_DLY11/clk_gate_cnt_wr_reg      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_0
core/fu_DLY11/clk_gate_r1reg_reg_0      1.6320     0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_1
core/fu_DLY11/rotreg_inst           1229.0048      1.1   176.3648  1018.3680      0.0000  rotreg_dataw32_reg_count10
core/fu_DLY11/rotreg_inst/clk_gate_q_o_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_1
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_2
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_0
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_4
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_22
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_6
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_23
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_8
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_24
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_10
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_25
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_12
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_26
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_14
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_27
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_16
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_28
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_18
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_29
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_20
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_30
core/fu_TFG                         2775.0529      2.5     7.9424    31.1168      0.0000  fu_tfg_always_6_dataw32_busw32_nexpw4
core/fu_TFG/clk_gate_o1tmp_reg_0       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_3
core/fu_TFG/clk_gate_r1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_1
core/fu_TFG/clk_gate_r1reg_reg_1_0      1.6320     0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_0
core/fu_TFG/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_4
core/fu_TFG/fu_arch                 2729.4657      2.5    28.6144   155.5840      0.0000  tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_0
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_1
core/fu_TFG/fu_arch/u0                36.9920      0.0    36.9920     0.0000      0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_1
core/fu_TFG/fu_arch/u1                 3.1552      0.0     3.1552     0.0000      0.0000  radix_2_stagew3_nexpw4
core/fu_TFG/fu_arch/u2                74.2016      0.1    74.2016     0.0000      0.0000  weight_idxw16_stagew3_nexpw4_lut_addrw12
core/fu_TFG/fu_arch/u3                78.6624      0.1    78.6624     0.0000      0.0000  scale_k_idxw16_stagew3
core/fu_TFG/fu_arch/u4                25.5680      0.0    25.5680     0.0000      0.0000  lut_addr_idxw16_lut_addrw12
core/fu_TFG/fu_arch/u5              2153.2609      1.9     0.0000     0.0000      0.0000  tf_lut_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u5/lut          2153.2609      1.9  2119.0977    34.1632      0.0000  distr_2049x32_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u6               170.1632      0.2    73.9840    96.1792      0.0000  twiddle_twist_dataw32_halfw16
core/fu_add                          259.8144      0.2    27.9616    99.2256      0.0000  fu_add_sub_always_1_dataw32_busw32
core/fu_add/clk_gate_o1temp_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_0
core/fu_add/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_1
core/fu_add/fu_arch                  129.3632      0.1   129.3632     0.0000      0.0000  add_sub_arith_dataw32
core/fu_lsu_r                        178.1056      0.2    52.7680   118.8096      0.0000  fu_ldw_stw_always_4_dataw32_addrw16_1
core/fu_lsu_r/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_4
core/fu_lsu_r/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_3
core/fu_lsu_r/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_1
core/fu_lsu_r/clk_gate_r1_reg_reg_0      1.6320     0.0    0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_0
core/fu_lsu_w                        176.6912      0.2    40.0384   130.1248      0.0000  fu_ldw_stw_always_4_dataw32_addrw16_0
core/fu_lsu_w/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_4
core/fu_lsu_w/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_3
core/fu_lsu_w/clk_gate_data_out_reg_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_0
core/fu_lsu_w/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_1
core/fu_sh                           270.5856      0.2    27.2000    96.1792      0.0000  fu_shl_shr_always_1_dataw32_shiftw5
core/fu_sh/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_0
core/fu_sh/clk_gate_o1temp_reg_0       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_1
core/fu_sh/fu_arch                   143.9424      0.1   143.9424     0.0000      0.0000  shl_shr_arith_dataw32_shiftw5
core/ic                             1255.4432      1.1    91.5008     0.0000      0.0000  tta0_interconn
core/ic/AG_i1                          7.8336      0.0     7.8336     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_13
core/ic/AG_i2                          1.7408      0.0     1.7408     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_1
core/ic/AG_i3                         13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_12
core/ic/AG_o1                         70.2848      0.1    27.8528    42.4320      0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_1
core/ic/CADD1_i1                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_11
core/ic/CADD1_i2                       0.4352      0.0     0.4352     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_01_DATAW1
core/ic/CADD1_o1                      18.2784      0.0    15.4496     2.8288      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_5
core/ic/CMUL_i1                       13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_10
core/ic/CMUL_i2                       20.7808      0.0    20.7808     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_9
core/ic/CMUL_o1                      104.4480      0.1    13.9264    90.5216      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_4
core/ic/DLY10_i1                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_8
core/ic/DLY10_o1                     107.2768      0.1    13.9264    93.3504      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_3
core/ic/TFG_i1                         7.8336      0.0     7.8336     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_7
core/ic/TFG_i2                         2.3936      0.0     2.3936     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_0
core/ic/TFG_o1                       104.4480      0.1    13.9264    90.5216      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_2
core/ic/TFG_o2                         6.0928      0.0     0.4352     5.6576      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_2
core/ic/add_i1                        13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_6
core/ic/add_i2                        14.0352      0.0    14.0352     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_5
core/ic/add_o1                       132.3008      0.1    38.9504    93.3504      0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_1
core/ic/gcu_i1                         8.0512      0.0     5.2224     2.8288      0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_1
core/ic/gcu_i2                        27.8528      0.0     5.2224    22.6304      0.0000  tta0_output_socket_cons_2_1_BUSW_08_BUSW_18_DATAW_06
core/ic/gcu_o1                         8.3776      0.0     5.5488     2.8288      0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_0
core/ic/iter                          13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_4
core/ic/lsu_r_i1                       6.0928      0.0     6.0928     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_1
core/ic/lsu_r_i2                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_3
core/ic/lsu_r_o1                     107.2768      0.1    13.9264    93.3504      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_1
core/ic/lsu_w_i1                       6.0928      0.0     6.0928     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_0
core/ic/lsu_w_i2                      14.4704      0.0    14.4704     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_2
core/ic/rf_def_i1                     13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_1
core/ic/rf_def_o1                     36.9920      0.0    31.3344     5.6576      0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_0
core/ic/sh_i1                          2.1760      0.0     2.1760     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW5
core/ic/sh_i2                         13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_0
core/ic/sh_o1                        135.1296      0.1    38.9504    96.1792      0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_0
core/ic/simm_socket_B1                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_4
core/ic/simm_socket_B10                7.0720      0.0     1.4144     5.6576      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_0
core/ic/simm_socket_B2                13.0560      0.0     1.7408    11.3152      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_3
core/ic/simm_socket_B4                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_2
core/ic/simm_socket_B5                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_1
core/ic/simm_socket_B6                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_0
core/ic/simm_socket_B8                 4.2432      0.0     1.4144     2.8288      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_1
core/inst_decoder                     91.2832      0.1    49.9392    28.2880      0.0000  tta0_decoder
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_7
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_6
core/inst_decoder/clk_gate_fu_gcu_opc_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_0
core/inst_decoder/clk_gate_simm_B1_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_5
core/inst_decoder/clk_gate_simm_B2_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_4
core/inst_decoder/clk_gate_simm_B4_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_3
core/inst_decoder/clk_gate_simm_B5_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_2
core/inst_decoder/clk_gate_simm_B6_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_1
core/inst_fetch                      261.1200      0.2   101.4016   149.9264      0.0000  tta0_ifetch
core/inst_fetch/clk_gate_instruction_reg_reg_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_0
core/inst_fetch/clk_gate_instruction_reg_reg_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_4
core/inst_fetch/clk_gate_instruction_reg_reg_1_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_5
core/inst_fetch/clk_gate_loop_iter_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_1
core/inst_fetch/clk_gate_loop_iter_temp_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_2
core/inst_fetch/clk_gate_return_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_3
core/loopbuffer                     1031.0976      0.9   350.6624   670.6432      0.0000  tta0_loopbuf_1_32_51_1_0
core/loopbuffer/clk_gate_iter_cntr_r_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_5
core/loopbuffer/clk_gate_len_cntr_r_reg_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_6
core/loopbuffer/clk_gate_len_cntr_r_reg_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_7
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_1
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_4
core/loopbuffer/clk_gate_loop_len_r_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_3
core/rf_rf_def                       346.6368      0.3    52.9856   288.7552      0.0000  rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3
core/rf_rf_def/clk_gate_reg_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_1
core/rf_rf_def/clk_gate_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_2
core/rf_rf_def/clk_gate_reg_reg[2]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_0
datamem/dmem_1                     47909.1165     43.1     3.0464     0.0000      0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_1
datamem/dmem_1/cacti_mem_0           262.9920      0.2     0.0000     0.0000    262.9920  cacti_mem_wrapper_DATAW32_ADDRW5_3
datamem/dmem_1/cacti_mem_x_1         262.9920      0.2     0.0000     0.0000    262.9920  cacti_mem_wrapper_DATAW32_ADDRW5_2
datamem/dmem_1/cacti_mem_x_2         460.5120      0.4     0.0000     0.0000    460.5120  cacti_mem_wrapper_DATAW32_ADDRW6_1
datamem/dmem_1/cacti_mem_x_3         811.7870      0.7     0.0000     0.0000    811.7870  cacti_mem_wrapper_DATAW32_ADDRW7_1
datamem/dmem_1/cacti_mem_x_4        1484.1500      1.3     0.0000     0.0000   1484.1500  cacti_mem_wrapper_DATAW32_ADDRW8_1
datamem/dmem_1/cacti_mem_x_5        3120.8401      2.8     0.0000     0.0000   3120.8401  cacti_mem_wrapper_DATAW32_ADDRW9_1
datamem/dmem_1/cacti_mem_x_6        5670.1802      5.1     0.0000     0.0000   5670.1802  cacti_mem_wrapper_DATAW32_ADDRW10_1
datamem/dmem_1/cacti_mem_x_7       11088.9004     10.0     0.0000     0.0000  11088.9004  cacti_mem_wrapper_DATAW32_ADDRW11_1
datamem/dmem_1/cacti_mem_x_8       24576.5996     22.1     0.0000     0.0000  24576.5996  cacti_mem_wrapper_DATAW32_ADDRW12_1
datamem/dmem_1/en_wr_arbiter          23.9360      0.0    23.9360     0.0000      0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_1
datamem/dmem_1/mem_output_sel        143.1808      0.1   117.7216    25.4592      0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_1
datamem/dmem_2                     47905.7437     43.1     0.8704     0.0000      0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_0
datamem/dmem_2/cacti_mem_0           262.9920      0.2     0.0000     0.0000    262.9920  cacti_mem_wrapper_DATAW32_ADDRW5_1
datamem/dmem_2/cacti_mem_x_1         262.9920      0.2     0.0000     0.0000    262.9920  cacti_mem_wrapper_DATAW32_ADDRW5_0
datamem/dmem_2/cacti_mem_x_2         460.5120      0.4     0.0000     0.0000    460.5120  cacti_mem_wrapper_DATAW32_ADDRW6_0
datamem/dmem_2/cacti_mem_x_3         811.7870      0.7     0.0000     0.0000    811.7870  cacti_mem_wrapper_DATAW32_ADDRW7_0
datamem/dmem_2/cacti_mem_x_4        1484.1500      1.3     0.0000     0.0000   1484.1500  cacti_mem_wrapper_DATAW32_ADDRW8_0
datamem/dmem_2/cacti_mem_x_5        3120.8401      2.8     0.0000     0.0000   3120.8401  cacti_mem_wrapper_DATAW32_ADDRW9_0
datamem/dmem_2/cacti_mem_x_6        5670.1802      5.1     0.0000     0.0000   5670.1802  cacti_mem_wrapper_DATAW32_ADDRW10_0
datamem/dmem_2/cacti_mem_x_7       11088.9004     10.0     0.0000     0.0000  11088.9004  cacti_mem_wrapper_DATAW32_ADDRW11_0
datamem/dmem_2/cacti_mem_x_8       24576.5996     22.1     0.0000     0.0000  24576.5996  cacti_mem_wrapper_DATAW32_ADDRW12_0
datamem/dmem_2/en_wr_arbiter          22.9568      0.0    22.9568     0.0000      0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_0
datamem/dmem_2/mem_output_sel        142.9632      0.1   117.5040    25.4592      0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_0
datamem/par_mem_logic_inst           841.6768      0.8     1.5232     0.0000      0.0000  par_mem_logic_PORTS2_LSU_ADDRW14_CTRLW1_DATAW32_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0    189.5296     0.2  103.0336   84.8640     0.0000 addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0/clk_gate_addrs_latch_reg[0]      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/ctrl_units_0     75.6160     0.1    9.1392    5.6576     0.0000 ctrl_units_PORTS2_CTRLW1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0     31.3344     0.0   12.7296   16.9728     0.0000 ctrl_unit_PORTS2_CTRLW1_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1     29.4848     0.0   13.7088   14.1440     0.0000 ctrl_unit_PORTS2_CTRLW1_0
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_0
datamem/par_mem_logic_inst/rd_crsbar_0    292.0192     0.3  102.0544  186.7008     0.0000 rd_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[1]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0    282.9888     0.3   98.6816  181.0432     0.0000 wr_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/scheduler                    709.3760      0.6   109.3440   268.7360      0.0000  par_mem_scheduler_dataw32_addrw14_ports2_rd_delay1
datamem/scheduler/addr_a_conv         18.6048      0.0    14.1440     2.8288      0.0000  ser_par_conv_portw14_port_cnt2_1
datamem/scheduler/addr_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_1
datamem/scheduler/addr_b_conv         52.1152      0.0     8.0512    42.4320      0.0000  ser_par_conv_portw14_port_cnt2_0
datamem/scheduler/addr_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_0
datamem/scheduler/d_a_conv            27.3088      0.0    22.8480     2.8288      0.0000  ser_par_conv_portw32_port_cnt2_3
datamem/scheduler/d_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_1
datamem/scheduler/d_b_conv           103.2512      0.1    16.7552    84.8640      0.0000  ser_par_conv_portw32_port_cnt2_1
datamem/scheduler/d_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_0
datamem/scheduler/par_ser_conv_inst    130.0160     0.1   27.7440    99.0080      0.0000  par_ser_conv_portw32_port_cnt2
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_1
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_2
instmem/cacti_mem                    709.0260      0.6     0.0000     0.0000    709.0260  cacti_mem_wrapper_DATAW51_ADDRW6
--------------------------------  -----------  -------  ---------  ---------  ----------  -----------------------------------------------------------------------------------
Total                                                   8635.6739  6405.9263  96186.9326

1
