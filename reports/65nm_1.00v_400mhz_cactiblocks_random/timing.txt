Information: Updating design information... (UID-85)
 
****************************************
Report : timing
        -path full
        -delay max
        -max_paths 10
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 19:54:25 2018
****************************************

 * Some/all delay information is back-annotated.

Operating Conditions: uk65lscllmvbbh_100c25_tc   Library: uk65lscllmvbbh_100c25_tc
Wire Load Model Mode: Inactive.

  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG460_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1171/Z (AOI21M4R)                0.053 *    2.294 r
  core/fu_CMUL/fu_arch/U3768/Z (XOR2M2RA)                0.101 *    2.396 f
  core/fu_CMUL/fu_arch/U66/Z (OAI21M4R)                  0.057 *    2.452 r
  core/fu_CMUL/fu_arch/P[26] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.452 r
  core/fu_CMUL/clk_r_REG460_S16/D (DFQRM2RA)             0.000 *    2.452 r
  data arrival time                                                 2.452

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG460_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.038      2.462
  data required time                                                2.462
  --------------------------------------------------------------------------
  data required time                                                2.462
  data arrival time                                                -2.452
  --------------------------------------------------------------------------
  slack (MET)                                                       0.010


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG442_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1251/Z (AOI21M3R)                0.057 *    2.299 r
  core/fu_CMUL/fu_arch/U3822/Z (XOR2M2RA)                0.103 *    2.402 f
  core/fu_CMUL/fu_arch/U61/Z (OAI21M4R)                  0.049 *    2.451 r
  core/fu_CMUL/fu_arch/P[24] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.451 r
  core/fu_CMUL/clk_r_REG442_S16/D (DFQRM2RA)             0.000 *    2.451 r
  data arrival time                                                 2.451

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG442_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.036      2.464
  data required time                                                2.464
  --------------------------------------------------------------------------
  data required time                                                2.464
  data arrival time                                                -2.451
  --------------------------------------------------------------------------
  slack (MET)                                                       0.013


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG424_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1238/Z (AOI21M4R)                0.052 *    2.294 r
  core/fu_CMUL/fu_arch/U3861/Z (XOR2M2RA)                0.094 *    2.387 f
  core/fu_CMUL/fu_arch/U1133/Z (OAI21M2R)                0.057 *    2.445 r
  core/fu_CMUL/fu_arch/P[22] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.445 r
  core/fu_CMUL/clk_r_REG424_S16/D (DFQRM2RA)             0.000 *    2.445 r
  data arrival time                                                 2.445

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG424_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.041      2.459
  data required time                                                2.459
  --------------------------------------------------------------------------
  data required time                                                2.459
  data arrival time                                                -2.445
  --------------------------------------------------------------------------
  slack (MET)                                                       0.014


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG451_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1196/Z (AOI21M3R)                0.056 *    2.298 r
  core/fu_CMUL/fu_arch/U3875/Z (XOR2M2RA)                0.102 *    2.400 f
  core/fu_CMUL/fu_arch/U67/Z (OAI21M4R)                  0.048 *    2.448 r
  core/fu_CMUL/fu_arch/P[25] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.448 r
  core/fu_CMUL/clk_r_REG451_S16/D (DFQRM2RA)             0.000 *    2.448 r
  data arrival time                                                 2.448

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG451_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.036      2.464
  data required time                                                2.464
  --------------------------------------------------------------------------
  data required time                                                2.464
  data arrival time                                                -2.448
  --------------------------------------------------------------------------
  slack (MET)                                                       0.016


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG478_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1222/Z (AOI21M4R)                0.054 *    2.296 r
  core/fu_CMUL/fu_arch/U3858/Z (XOR2M2RA)                0.100 *    2.396 f
  core/fu_CMUL/fu_arch/U3873/Z (OAI22M4R)                0.045 *    2.441 r
  core/fu_CMUL/fu_arch/P[28] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.441 r
  core/fu_CMUL/clk_r_REG478_S16/D (DFQRM2RA)             0.000 *    2.441 r
  data arrival time                                                 2.441

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG478_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.042      2.458
  data required time                                                2.458
  --------------------------------------------------------------------------
  data required time                                                2.458
  data arrival time                                                -2.441
  --------------------------------------------------------------------------
  slack (MET)                                                       0.017


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG433_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U1284/Z (AOI21M3R)                0.056 *    2.298 r
  core/fu_CMUL/fu_arch/U3763/Z (XOR2M2RA)                0.102 *    2.400 f
  core/fu_CMUL/fu_arch/U3735/Z (OAI22M4R)                0.042 *    2.442 r
  core/fu_CMUL/fu_arch/P[23] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.442 r
  core/fu_CMUL/clk_r_REG433_S16/D (DFQRM2RA)             0.000 *    2.442 r
  data arrival time                                                 2.442

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG433_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.040      2.460
  data required time                                                2.460
  --------------------------------------------------------------------------
  data required time                                                2.460
  data arrival time                                                -2.442
  --------------------------------------------------------------------------
  slack (MET)                                                       0.018


  Startpoint: core/fu_CMUL/fu_arch/R_12
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG33_S5
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_12/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_12/Q (DFQRM8RA)                 0.282      0.292 r
  core/fu_CMUL/fu_arch/U2930/Z (XNR2M2SA)                0.259 *    0.551 f
  core/fu_CMUL/fu_arch/U3051/Z (AO21M2SA)                0.216 *    0.767 f
  core/fu_CMUL/fu_arch/U3066/Z (XNR3M2S)                 0.452 *    1.219 r
  core/fu_CMUL/fu_arch/U3112/CO (ADFM2RA)                0.152 *    1.371 r
  core/fu_CMUL/fu_arch/U3074/Z (INVM2S)                  0.053 *    1.424 f
  core/fu_CMUL/fu_arch/U3076/Z (OAI21M4R)                0.074 *    1.499 r
  core/fu_CMUL/fu_arch/U3078/Z (XOR2M2RA)                0.144 *    1.643 f
  core/fu_CMUL/fu_arch/U3085/Z (XOR2M2RA)                0.107 *    1.750 r
  core/fu_CMUL/fu_arch/U3168/CO (ADHM2RA)                0.081 *    1.831 r
  core/fu_CMUL/fu_arch/U3169/Z (OR2M4R)                  0.080 *    1.911 r
  core/fu_CMUL/fu_arch/U3170/Z (ND2M4R)                  0.053 *    1.965 f
  core/fu_CMUL/fu_arch/U3171/Z (NR2M4R)                  0.065 *    2.030 r
  core/fu_CMUL/fu_arch/U3179/Z (CKND2M4R)                0.063 *    2.093 f
  core/fu_CMUL/fu_arch/U3189/Z (NR2M6R)                  0.066 *    2.159 r
  core/fu_CMUL/fu_arch/U3190/Z (ND2M6R)                  0.056 *    2.215 f
  core/fu_CMUL/fu_arch/U3279/Z (NR2M4R)                  0.060 *    2.276 r
  core/fu_CMUL/fu_arch/U3280/Z (AOI22M4R)                0.046 *    2.322 f
  core/fu_CMUL/fu_arch/U3282/Z (CKND2M4R)                0.042 *    2.364 r
  core/fu_CMUL/fu_arch/U1143/Z (AOI21M3R)                0.028 *    2.393 f
  core/fu_CMUL/fu_arch/U3659/Z (OAI21M4R)                0.049 *    2.442 r
  core/fu_CMUL/fu_arch/P[14] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.442 r
  core/fu_CMUL/clk_r_REG33_S5/D (DFQRM2RA)               0.000 *    2.442 r
  data arrival time                                                 2.442

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG33_S5/CK (DFQRM2RA)              0.000      2.500 r
  library setup time                                    -0.036      2.464
  data required time                                                2.464
  --------------------------------------------------------------------------
  data required time                                                2.464
  data arrival time                                                -2.442
  --------------------------------------------------------------------------
  slack (MET)                                                       0.022


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG379_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U124/Z (AOI21M6R)                 0.080 *    2.217 r
  core/fu_CMUL/fu_arch/U3746/Z (NR2B1M4R)                0.036 *    2.253 f
  core/fu_CMUL/fu_arch/U1134/Z (AOI22M4R)                0.045 *    2.298 r
  core/fu_CMUL/fu_arch/U1121/Z (ND2M2R)                  0.040 *    2.338 f
  core/fu_CMUL/fu_arch/U2706/Z (ND2M2R)                  0.039 *    2.376 r
  core/fu_CMUL/fu_arch/U3903/Z (ND2M4R)                  0.032 *    2.409 f
  core/fu_CMUL/fu_arch/U2708/Z (OAI21M4R)                0.029 *    2.438 r
  core/fu_CMUL/fu_arch/P[30] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.438 r
  core/fu_CMUL/clk_r_REG379_S16/D (DFQRM2RA)             0.000 *    2.438 r
  data arrival time                                                 2.438

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG379_S16/CK (DFQRM2RA)            0.000      2.500 r
  library setup time                                    -0.035      2.465
  data required time                                                2.465
  --------------------------------------------------------------------------
  data required time                                                2.465
  data arrival time                                                -2.438
  --------------------------------------------------------------------------
  slack (MET)                                                       0.027


  Startpoint: core/fu_CMUL/fu_arch/R_12
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG192_S5
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_12/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_12/Q (DFQRM8RA)                 0.282      0.292 r
  core/fu_CMUL/fu_arch/U2930/Z (XNR2M2SA)                0.259 *    0.551 f
  core/fu_CMUL/fu_arch/U3051/Z (AO21M2SA)                0.216 *    0.767 f
  core/fu_CMUL/fu_arch/U3066/Z (XNR3M2S)                 0.452 *    1.219 r
  core/fu_CMUL/fu_arch/U3112/CO (ADFM2RA)                0.152 *    1.371 r
  core/fu_CMUL/fu_arch/U3074/Z (INVM2S)                  0.053 *    1.424 f
  core/fu_CMUL/fu_arch/U3076/Z (OAI21M4R)                0.074 *    1.499 r
  core/fu_CMUL/fu_arch/U3078/Z (XOR2M2RA)                0.144 *    1.643 f
  core/fu_CMUL/fu_arch/U3085/Z (XOR2M2RA)                0.107 *    1.750 r
  core/fu_CMUL/fu_arch/U3168/CO (ADHM2RA)                0.081 *    1.831 r
  core/fu_CMUL/fu_arch/U3169/Z (OR2M4R)                  0.080 *    1.911 r
  core/fu_CMUL/fu_arch/U3170/Z (ND2M4R)                  0.053 *    1.965 f
  core/fu_CMUL/fu_arch/U3406/Z (NR2M4S)                  0.106 *    2.071 r
  core/fu_CMUL/fu_arch/U3407/Z (CKND2M4R)                0.074 *    2.145 f
  core/fu_CMUL/fu_arch/U3408/Z (NR2M4S)                  0.096 *    2.241 r
  core/fu_CMUL/fu_arch/U99/Z (AOI21M4R)                  0.059 *    2.300 f
  core/fu_CMUL/fu_arch/U3784/Z (XOR2M2RA)                0.075 *    2.375 f
  core/fu_CMUL/fu_arch/U1162/Z (OAI22M2R)                0.050 *    2.425 r
  core/fu_CMUL/fu_arch/P[12] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.425 r
  core/fu_CMUL/clk_r_REG192_S5/D (DFQRM2RA)              0.000 *    2.425 r
  data arrival time                                                 2.425

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG192_S5/CK (DFQRM2RA)             0.000      2.500 r
  library setup time                                    -0.046      2.454
  data required time                                                2.454
  --------------------------------------------------------------------------
  data required time                                                2.454
  data arrival time                                                -2.425
  --------------------------------------------------------------------------
  slack (MET)                                                       0.029


  Startpoint: core/fu_CMUL/fu_arch/R_27
              (rising edge-triggered flip-flop clocked by clk)
  Endpoint: core/fu_CMUL/clk_r_REG469_S16
            (rising edge-triggered flip-flop clocked by clk)
  Path Group: clk
  Path Type: max

  Point                                                   Incr       Path
  --------------------------------------------------------------------------
  clock clk (rise edge)                                  0.000      0.000
  clock network delay (ideal)                            0.010      0.010
  core/fu_CMUL/fu_arch/R_27/CK (DFQRM8RA)                0.000      0.010 r
  core/fu_CMUL/fu_arch/R_27/Q (DFQRM8RA)                 0.272      0.282 r
  core/fu_CMUL/fu_arch/U3885/Z (XOR2M8RA)                0.203 *    0.485 f
  core/fu_CMUL/fu_arch/U909/Z (NR2B1M2S)                 0.100 *    0.585 r
  core/fu_CMUL/fu_arch/U919/S (ADHM1SA)                  0.227 *    0.812 r
  core/fu_CMUL/fu_arch/U937/S (ADFM2SA)                  0.354 *    1.166 f
  core/fu_CMUL/fu_arch/U938/Z (NR2M2S)                   0.092 *    1.258 r
  core/fu_CMUL/fu_arch/U939/Z (INVM2S)                   0.056 *    1.314 f
  core/fu_CMUL/fu_arch/U942/Z (AOI21M2S)                 0.102 *    1.416 r
  core/fu_CMUL/fu_arch/U947/Z (OAI21M2R)                 0.067 *    1.484 f
  core/fu_CMUL/fu_arch/U948/Z (INVM2S)                   0.046 *    1.529 r
  core/fu_CMUL/fu_arch/U953/Z (OAI21M2R)                 0.051 *    1.580 f
  core/fu_CMUL/fu_arch/U956/Z (AOI21B10M2R)              0.076 *    1.656 r
  core/fu_CMUL/fu_arch/U958/Z (OAI21M2R)                 0.066 *    1.723 f
  core/fu_CMUL/fu_arch/U967/Z (AOI21M3R)                 0.083 *    1.806 r
  core/fu_CMUL/fu_arch/U973/Z (OAI21M4R)                 0.062 *    1.868 f
  core/fu_CMUL/fu_arch/U3023/Z (AOI21M8R)                0.072 *    1.940 r
  core/fu_CMUL/fu_arch/U1402/Z (OAI21M12RA)              0.063 *    2.004 f
  core/fu_CMUL/fu_arch/U1021/Z (AOI21M16RA)              0.078 *    2.082 r
  core/fu_CMUL/fu_arch/U1350/Z (OAI21M12RA)              0.055 *    2.137 f
  core/fu_CMUL/fu_arch/U1122/Z (AOI21M8R)                0.065 *    2.202 r
  core/fu_CMUL/fu_arch/U1123/Z (INVM12R)                 0.040 *    2.242 f
  core/fu_CMUL/fu_arch/U3809/Z (AOI21M4R)                0.054 *    2.296 r
  core/fu_CMUL/fu_arch/U3860/Z (XOR2M2RA)                0.100 *    2.396 f
  core/fu_CMUL/fu_arch/U3810/Z (OAI22M4R)                0.046 *    2.442 r
  core/fu_CMUL/fu_arch/P[27] (cmul_pipe_1_dataw32_halfw16)
                                                         0.000      2.442 r
  core/fu_CMUL/clk_r_REG469_S16/D (DFQRM2WA)             0.000 *    2.442 r
  data arrival time                                                 2.442

  clock clk (rise edge)                                  2.500      2.500
  clock network delay (ideal)                            0.010      2.510
  clock uncertainty                                     -0.010      2.500
  core/fu_CMUL/clk_r_REG469_S16/CK (DFQRM2WA)            0.000      2.500 r
  library setup time                                    -0.028      2.472
  data required time                                                2.472
  --------------------------------------------------------------------------
  data required time                                                2.472
  data arrival time                                                -2.442
  --------------------------------------------------------------------------
  slack (MET)                                                       0.029


1
