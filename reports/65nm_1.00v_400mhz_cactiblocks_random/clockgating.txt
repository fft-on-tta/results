 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 19:54:23 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2215 (98.97%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    23 (1.03%)    |
          |                                       |                  |
          |    Total number of registers          |     2238         |
          ------------------------------------------------------------



1
