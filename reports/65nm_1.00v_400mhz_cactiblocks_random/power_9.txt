Information: Propagating switching activity (high effort zero delay simulation). (PWR-6)
Warning: Design has unannotated sequential cell outputs. (PWR-415)
Warning: Design has unannotated black box outputs. (PWR-428)
 
****************************************
Report : power
        -analysis_effort high
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 19:54:50 2018
****************************************


Library(s) Used:

    uk65lscllmvbbh_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db)
    uk65lscllmvbbr_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbr_100c25_tc.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51/ram_64x51.db)
    uk65lscllmvbbl_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbl_100c25_tc.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32/ram_32x32.db)


Operating Conditions: uk65lscllmvbbh_100c25_tc   Library: uk65lscllmvbbh_100c25_tc
Wire Load Model Mode: Inactive.


Global Operating Voltage = 1    
Power-specific unit information :
    Voltage Units = 1V
    Capacitance Units = 1.000000pf
    Time Units = 1ns
    Dynamic Power Units = 1mW    (derived from V,C,T units)
    Leakage Power Units = 1pW

Warning: Cannot report correlated power unless power prediction mode is set. (PWR-727)
Power Breakdown
---------------

                             Cell        Driven Net  Tot Dynamic      Cell
                             Internal    Switching   Power (mW)       Leakage
Cell                         Power (mW)  Power (mW)  (% Cell/Tot)     Power (pW)
--------------------------------------------------------------------------------
Netlist Power                   6.4954      4.1428   1.064e+01 (61%)  1.597e+09
Estimated Clock Tree Power   N/A         N/A          (N/A)           N/A
--------------------------------------------------------------------------------

                 Internal         Switching           Leakage            Total
Power Group      Power            Power               Power              Power   (   %    )  Attrs
--------------------------------------------------------------------------------------------------
io_pad             0.0000            0.0000            0.0000            0.0000  (   0.00%)
memory             0.6430        2.5954e-02        1.5959e+09            2.2649  (  18.51%)
black_box          0.0000            0.0000            0.0000            0.0000  (   0.00%)
clock_network      0.2596            0.4804        5.8953e+03            0.7400  (   6.05%)
register           2.7666            0.3751        3.3130e+05            3.1420  (  25.68%)
sequential         0.0000            0.0000            0.0000            0.0000  (   0.00%)
combinational      2.8262            3.2614        1.0431e+06            6.0886  (  49.76%)
--------------------------------------------------------------------------------------------------
Total              6.4954 mW         4.1428 mW     1.5973e+09 pW        12.2355 mW
1
