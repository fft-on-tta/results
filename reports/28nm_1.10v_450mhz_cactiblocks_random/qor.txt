 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 06:10:42 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              43.00
  Critical Path Length:          2.18
  Critical Path Slack:           0.00
  Critical Path Clk Period:      2.22
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        210
  Hierarchical Port Count:      11253
  Leaf Cell Count:              16522
  Buf/Inv Cell Count:            3078
  Buf Cell Count:                 859
  Inv Cell Count:                2219
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14235
  Sequential Cell Count:         2287
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7349.657880
  Noncombinational Area:  6227.167921
  Buf/Inv Area:            858.323212
  Total Buffer Area:           375.47
  Total Inverter Area:         482.85
  Macro/Black Box Area:  90431.190369
  Net Area:                  0.000000
  Net XLength        :       97208.03
  Net YLength        :       88911.45
  -----------------------------------
  Cell Area:            104008.016170
  Design Area:          104008.016170
  Net Length        :       186119.48


  Design Rules
  -----------------------------------
  Total Number of Nets:         18373
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  242.32
  Logic Optimization:                 11.55
  Mapping Optimization:               57.01
  -----------------------------------------
  Overall Compile Time:              502.93
  Overall Compile Wall Clock Time:   450.96

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
