 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 06:10:41 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |       99         |
          |                                       |                  |
          |    Number of Gated registers          |  2152 (99.22%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.78%)    |
          |                                       |                  |
          |    Total number of registers          |     2169         |
          ------------------------------------------------------------



1
