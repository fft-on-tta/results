Information: Propagating switching activity (high effort zero delay simulation). (PWR-6)
Warning: Invalid switching activity annotation on constant net(s) is being ignored. (PWR-421)
Warning: Design has unannotated sequential cell outputs. (PWR-415)
Warning: Design has unannotated black box outputs. (PWR-428)
 
****************************************
Report : power
        -analysis_effort high
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 02:33:30 2018
****************************************


Library(s) Used:

    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP4_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP10_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x51/ram_64x51.db)
    C28SOI_SC_8_CORE_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs/C28SOI_SC_8_CORE_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_32x32/ram_32x32.db)


Operating Conditions: tt28_0.95V_0.00V_0.00V_0.00V_25C   Library: C28SOI_SC_8_CLK_LL
Wire Load Model Mode: Inactive.


Global Operating Voltage = 0.95 
Power-specific unit information :
    Voltage Units = 1V
    Capacitance Units = 1.000000pf
    Time Units = 1ns
    Dynamic Power Units = 1mW    (derived from V,C,T units)
    Leakage Power Units = 1mW

Warning: Cannot report correlated power unless power prediction mode is set. (PWR-727)
Power Breakdown
---------------

                             Cell        Driven Net  Tot Dynamic      Cell
                             Internal    Switching   Power (mW)       Leakage
Cell                         Power (mW)  Power (mW)  (% Cell/Tot)     Power (mW)
--------------------------------------------------------------------------------
Netlist Power                   7.6116      5.8906     13.5022 (56%)    19.2721
Estimated Clock Tree Power   N/A         N/A          (N/A)           N/A
--------------------------------------------------------------------------------

                 Internal         Switching           Leakage            Total
Power Group      Power            Power               Power              Power   (   %    )  Attrs
--------------------------------------------------------------------------------------------------
io_pad             0.0000            0.0000            0.0000            0.0000  (   0.00%)
memory             0.5981        3.3335e-02           19.2619           19.8933  (  60.70%)
black_box          0.0000            0.0000            0.0000            0.0000  (   0.00%)
clock_network      0.1736            0.7669        3.7932e-04            0.9409  (   2.87%)
register           3.6462            0.1866        3.1205e-03            3.8360  (  11.70%)
sequential         0.0000            0.0000            0.0000            0.0000  (   0.00%)
combinational      3.1937            4.9037        6.7068e-03            8.1041  (  24.73%)
--------------------------------------------------------------------------------------------------
Total              7.6117 mW         5.8906 mW        19.2721 mW        32.7743 mW
1
