 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 23:05:47 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              41.00
  Critical Path Length:          2.70
  Critical Path Slack:           0.11
  Critical Path Clk Period:      2.86
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11232
  Leaf Cell Count:              16465
  Buf/Inv Cell Count:            3055
  Buf Cell Count:                 848
  Inv Cell Count:                2207
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14176
  Sequential Cell Count:         2289
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7359.667475
  Noncombinational Area:  6231.628721
  Buf/Inv Area:            851.904012
  Total Buffer Area:           371.55
  Total Inverter Area:         480.35
  Macro/Black Box Area:  96186.932556
  Net Area:                  0.000000
  Net XLength        :       96133.82
  Net YLength        :       99317.44
  -----------------------------------
  Cell Area:            109778.228751
  Design Area:          109778.228751
  Net Length        :       195451.25


  Design Rules
  -----------------------------------
  Total Number of Nets:         18317
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  247.07
  Logic Optimization:                 11.20
  Mapping Optimization:               63.69
  -----------------------------------------
  Overall Compile Time:              513.62
  Overall Compile Wall Clock Time:   462.59

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
