Warning: There are 83 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 23:05:48 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         287(65.38%)       0(0.00%)          152(34.62%)        439
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         260(37.41%)       0(0.00%)          435(62.59%)        695
--------------------------------------------------------------------------------
1
