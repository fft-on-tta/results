 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 17:57:55 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              43.00
  Critical Path Length:          1.59
  Critical Path Slack:           0.03
  Critical Path Clk Period:      1.67
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11228
  Leaf Cell Count:              17465
  Buf/Inv Cell Count:            3180
  Buf Cell Count:                 861
  Inv Cell Count:                2319
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     15162
  Sequential Cell Count:         2303
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8223.865846
  Noncombinational Area:  6272.972721
  Buf/Inv Area:            935.027214
  Total Buffer Area:           411.81
  Total Inverter Area:         523.22
  Macro/Black Box Area:  96178.389893
  Net Area:                  0.000000
  Net XLength        :      104110.57
  Net YLength        :      110659.70
  -----------------------------------
  Cell Area:            110675.228460
  Design Area:          110675.228460
  Net Length        :       214770.27


  Design Rules
  -----------------------------------
  Total Number of Nets:         19281
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  261.68
  Logic Optimization:                 24.22
  Mapping Optimization:              116.30
  -----------------------------------------
  Overall Compile Time:              592.36
  Overall Compile Wall Clock Time:   500.98

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
