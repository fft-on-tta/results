 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 17:57:53 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2167 (99.22%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.78%)    |
          |                                       |                  |
          |    Total number of registers          |     2184         |
          ------------------------------------------------------------



1
