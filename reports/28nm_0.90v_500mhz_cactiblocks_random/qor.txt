 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 17:21:23 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              25.00
  Critical Path Length:          1.41
  Critical Path Slack:           0.06
  Critical Path Clk Period:      2.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11228
  Leaf Cell Count:              17553
  Buf/Inv Cell Count:            3279
  Buf Cell Count:                 896
  Inv Cell Count:                2383
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     15277
  Sequential Cell Count:         2276
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7775.718666
  Noncombinational Area:  6195.724721
  Buf/Inv Area:            932.524814
  Total Buffer Area:           403.54
  Total Inverter Area:         528.99
  Macro/Black Box Area:  96178.389893
  Net Area:                  0.000000
  Net XLength        :       99500.70
  Net YLength        :      109010.38
  -----------------------------------
  Cell Area:            110149.833280
  Design Area:          110149.833280
  Net Length        :       208511.08


  Design Rules
  -----------------------------------
  Total Number of Nets:         19263
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  261.62
  Logic Optimization:                 16.98
  Mapping Optimization:              126.05
  -----------------------------------------
  Overall Compile Time:              597.95
  Overall Compile Wall Clock Time:   500.61

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
