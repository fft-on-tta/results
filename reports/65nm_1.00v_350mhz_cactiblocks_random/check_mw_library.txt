
#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              uk65lscllmvbbh_100c25_tc
    File name                 /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db
    Library type              non-pg_pin based db
    Library Version           Not Specified
    Tool Created              B-2008.09-SP1-1
    Data Created              Not Specified
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1pW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:05 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (uk65lscllmvbbh_100c25_tc):
No power management cells in library#1.

#END_LIBSCREEN_UPF

#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY


#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              uk65lscllmvbbr_100c25_tc
    File name                 /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbr_100c25_tc.db
    Library type              non-pg_pin based db
    Library Version           Not Specified
    Tool Created              B-2008.09-SP1-1
    Data Created              Not Specified
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1pW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:06 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (uk65lscllmvbbr_100c25_tc):
No power management cells in library#1.

#END_LIBSCREEN_UPF

#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY


#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              uk65lscllmvbbl_100c25_tc
    File name                 /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbl_100c25_tc.db
    Library type              non-pg_pin based db
    Library Version           Not Specified
    Tool Created              B-2008.09-SP1-1
    Data Created              Not Specified
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1pW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:06 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (uk65lscllmvbbl_100c25_tc):
No power management cells in library#1.

#END_LIBSCREEN_UPF

#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_64x51 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_64x51
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51/ram_64x51.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_64x51):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_64x51(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_32x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_32x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32/ram_32x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_32x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_32x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_64x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_64x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32/ram_64x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_64x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_64x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_128x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_128x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32/ram_128x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_128x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_128x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_256x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_256x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32/ram_256x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_256x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_256x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_512x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_512x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32/ram_512x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_512x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_512x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_1024x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_1024x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32/ram_1024x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_1024x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_1024x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_2048x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_2048x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32/ram_2048x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_2048x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_2048x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_4096x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_4096x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32/ram_4096x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_4096x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_4096x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Checking library or object ram_8192x32 ...

#BEGIN_XCHECK_LIBRARY

Logic Library #1:
    Library name              ram_8192x32
    File name                 /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_8192x32/ram_8192x32.db
    Library type              non-pg_pin based db
    Library Version           2015_Spr
    Tool Created              I-2013.12
    Data Created              February 06, 2015
    Time unit                 1ns
    Capacitance unit          1000ff
    Leakage power unit        1mW
    Current unit              1mA
check_library options         
Version                       M-2017.06-SP2
Check date and time           Fri Jul 27 19:18:07 2018


#BEGIN_LIBSCREEN_UPF

Library#1 (ram_8192x32):
No power management cells in library#1.

#END_LIBSCREEN_UPF


Information: List of cell classification (LIBCHK-312)
------------------------------------------------------------------------------
Library name                ram_8192x32(lib#1)
------------------------------------------------------------------------------
Total number                  1
Inverter                      0
Buffer                        0
Level shifter                 0
Differential level shifter    0
Isolation cell                0
Clock Isolation cell          0
Retention cell                0
Switch cell                   0
Always on cell                0
------------------------------------------------------------------------------
#BEGIN_XCHECK_LOGICCELLS


#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_LOGICPINS



#END_XCHECK_LOGICPINS

#BEGIN_XCHECK_LOGICPGPINS

Library 1 is non-pg_pin library.

#END_XCHECK_LOGICPGPINS

#BEGIN_XCHECK_ARCS


#END_XCHECK_ARCS


#END_XCHECK_LIBRARY

Memory usage for this session 442 Mbytes.
CPU usage for this session 3 seconds ( 0.00 hours ).

Thank you...
Loading db file '/home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db'
Loading db file '/home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbr_100c25_tc.db'
Loading db file '/home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbl_100c25_tc.db'

#BEGIN_XCHECK_LIBRARY

Logic Library:    uk65lscllmvbbh_100c25_tc 
                  uk65lscllmvbbr_100c25_tc 
                  uk65lscllmvbbl_100c25_tc 
                  ram_64x51 
                  ram_32x32 
                  ram_64x32 
                  ram_128x32 
                  ram_256x32 
                  ram_512x32 
                  ram_1024x32 
                  ram_2048x32 
                  ram_4096x32 
                  ram_8192x32 
Physical Library: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/milkyway/uk65lscllmvbbh 
                  /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/milkyway/uk65lscllmvbbr 
                  /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/milkyway/uk65lscllmvbbl 
check_library options: 	
Version: 				M-2016.12-SP5-1
Check date and time:	Fri Jul 27 19:18:13 2018

          List of logic library and file names
------------------------------------------------------------------------------
Logic library name           Logic library file name
------------------------------------------------------------------------------
uk65lscllmvbbh_100c25_tc     /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db
uk65lscllmvbbr_100c25_tc     /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbr_100c25_tc.db
uk65lscllmvbbl_100c25_tc     /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbl_100c25_tc.db
ram_64x51                    /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51/ram_64x51.db
ram_32x32                    /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32/ram_32x32.db
ram_64x32                    /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32/ram_64x32.db
ram_128x32                   /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32/ram_128x32.db
ram_256x32                   /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32/ram_256x32.db
ram_512x32                   /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32/ram_512x32.db
ram_1024x32                  /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32/ram_1024x32.db
ram_2048x32                  /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32/ram_2048x32.db
ram_4096x32                  /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32/ram_4096x32.db
ram_8192x32                  /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_8192x32/ram_8192x32.db
------------------------------------------------------------------------------

#BEGIN_XCHECK_LOGICCELLS

Number of cells missing in logic library:	0 (out of 3241)

Information: List of physical only cells (LIBCHK-119)
-------------------------------------------------------------------------
Cell name                  Cell type             Physical library
-------------------------------------------------------------------------
WT3S                       PGPinOnly             uk65lscllmvbbh
WTBB3S                     PGPinOnly             uk65lscllmvbbh
WTBN3S                     PGPinOnly             uk65lscllmvbbh
WTBP3S                     PGPinOnly             uk65lscllmvbbh
FIL8S                      Filler                uk65lscllmvbbh
FIL16S                     Filler                uk65lscllmvbbh
FIL1S                      Filler                uk65lscllmvbbh
FIL2S                      Filler                uk65lscllmvbbh
FIL32S                     Filler                uk65lscllmvbbh
FIL4S                      Filler                uk65lscllmvbbh
FIL64S                     Filler                uk65lscllmvbbh
WT3R                       PGPinOnly             uk65lscllmvbbr
WTBB3R                     PGPinOnly             uk65lscllmvbbr
WTBN3R                     PGPinOnly             uk65lscllmvbbr
WTBP3R                     PGPinOnly             uk65lscllmvbbr
FIL8R                      Filler                uk65lscllmvbbr
FIL16R                     Filler                uk65lscllmvbbr
FIL1R                      Filler                uk65lscllmvbbr
FIL2R                      Filler                uk65lscllmvbbr
FIL32R                     Filler                uk65lscllmvbbr
FIL4R                      Filler                uk65lscllmvbbr
FIL64R                     Filler                uk65lscllmvbbr
WT3W                       PGPinOnly             uk65lscllmvbbl
WTBB3W                     PGPinOnly             uk65lscllmvbbl
WTBN3W                     PGPinOnly             uk65lscllmvbbl
WTBP3W                     PGPinOnly             uk65lscllmvbbl
FIL8W                      Filler                uk65lscllmvbbl
FIL16W                     Filler                uk65lscllmvbbl
FIL1W                      Filler                uk65lscllmvbbl
FIL2W                      Filler                uk65lscllmvbbl
FIL32W                     Filler                uk65lscllmvbbl
FIL4W                      Filler                uk65lscllmvbbl
FIL64W                     Filler                uk65lscllmvbbl
-------------------------------------------------------------------------

#END_XCHECK_LOGICCELLS

#BEGIN_XCHECK_PHYSICALCELLS

Number of cells missing in physical library:	10 (out of 3264)

Warning: List of cells missing in physical library (LIBCHK-211)
-------------------------------------------------------------------------
Cell name                  Cell type             Logic library
-------------------------------------------------------------------------
ram_64x51                  non pad_cell          ram_64x51
ram_32x32                  non pad_cell          ram_32x32
ram_64x32                  non pad_cell          ram_64x32
ram_128x32                 non pad_cell          ram_128x32
ram_256x32                 non pad_cell          ram_256x32
ram_512x32                 non pad_cell          ram_512x32
ram_1024x32                non pad_cell          ram_1024x32
ram_2048x32                non pad_cell          ram_2048x32
ram_4096x32                non pad_cell          ram_4096x32
ram_8192x32                non pad_cell          ram_8192x32
-------------------------------------------------------------------------

#END_XCHECK_PHYSICALCELLS

#BEGIN_XCHECK_PINS

Number of cells with missing or mismatched pins in libraries:	0

#END_XCHECK_PINS

#BEGIN_XCHECK_CELLANTENNADIODETYPE

Number of cells with inconsistent antenna_diode_type:	0

#END_XCHECK_CELLANTENNADIODETYPE

Logic vs. physical library check summary:
Number of cells missing in physical library:	10 
Information: Logic library is INCONSISTENT with physical library (LIBCHK-220)

#END_XCHECK_LIBRARY

0
