 
****************************************
Report : area
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 17:39:40 2018
****************************************

Library(s) Used:

    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.90V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP10_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.90V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.90V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_64x51/ram_64x51.db)
    C28SOI_SC_8_COREPBP4_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.90V_0.00V_0.00V_0.00V_25C.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_90v_32x32/ram_32x32.db)

Number of ports:                        11457
Number of nets:                         28825
Number of cells:                        17901
Number of combinational cells:          15009
Number of sequential cells:              2662
Number of macros/black boxes:              19
Number of buf/inv:                       3187
Number of references:                      49

Combinational area:               8140.089848
Buf/Inv area:                      935.462414
Noncombinational area:            6272.537521
Macro/Black Box area:            96178.389893
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                110591.017261
Total area:                 undefined

Hierarchical area distribution
------------------------------

                                  Global cell area              Local cell area
                                  --------------------  -------------------------------- 
Hierarchical cell                 Absolute     Percent  Combi-     Noncombi-  Black-
                                  Total        Total    national   national   boxes       Design
--------------------------------  -----------  -------  ---------  ---------  ----------  -----------------------------------------------------------------------------------
proc                              110591.0173    100.0    76.2688   138.6112      0.0000  proc
clk_gate_cnt_rd_reg                    1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0_4_0
clk_gate_en_blocks_reg_reg             1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_block_mem_output_sel_DATAW32_NBLOCKS9_1_2
clk_gate_tf_reg                        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16_0
core/decomp                           22.6304      0.0    22.6304     0.0000      0.0000  tta0_decompressor
core/fu_AG                           559.5584      0.5    33.4016   115.9808      0.0000  fu_ag_always_2_dataw32_busw32_nexpw4
core/fu_AG/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_5
core/fu_AG/clk_gate_o1tmp_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_3
core/fu_AG/clk_gate_o2reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_4
core/fu_AG/clk_gate_o2tmp_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_2
core/fu_AG/clk_gate_r1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_1
core/fu_AG/clk_gate_t1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0
core/fu_AG/fu_arch                   400.3840      0.4   218.9056    96.1792      0.0000  ag_arith_dataw32_nexpw4_idxw16_stagew3_mau_shift2
core/fu_AG/fu_arch/u0                 85.2992      0.1    34.3808    50.9184      0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_0
core/fu_CADD1                       1273.5040      1.2     0.4352     0.0000      0.0000  fu_cadd_always_1_dataw32_busw32
core/fu_CADD1/fu_arch               1273.0688      1.2     0.0000     0.0000      0.0000  cadd_full_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_arith    918.4896     0.8  609.7152  308.7744     0.0000  cadd_arith_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_reg    354.5792     0.3   59.8400   280.0512      0.0000  cadd_reg_dataw32
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_8
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_9
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_4
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_0
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_1
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_2      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_2
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_3      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_3
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_7
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_c_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_6
core/fu_CMUL                        2718.5857      2.5    43.6288   189.5296      0.0000  fu_cmul_always_3_dataw32_busw32
core/fu_CMUL/clk_gate_o1reg_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_0
core/fu_CMUL/clk_gate_o1reg_reg_0_0      1.6320     0.0    0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_3
core/fu_CMUL/clk_gate_o1tmp_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_2
core/fu_CMUL/clk_gate_r1reg_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_1
core/fu_CMUL/fu_arch                2478.8993      2.2  2296.0065   181.2608      0.0000  cmul_pipe_1_dataw32_halfw16
core/fu_CMUL/fu_arch/clk_gate_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16
core/fu_DLY11                       1269.2608      1.1     0.6528   107.7120      0.0000  fu_rotreg_always_11_dataw32_busw32
core/fu_DLY11/clk_gate_cnt_wr_reg      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_0
core/fu_DLY11/clk_gate_r1reg_reg_0      1.6320     0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_1
core/fu_DLY11/rotreg_inst           1157.6320      1.0   167.2256   956.1344      0.0000  rotreg_dataw32_reg_count10
core/fu_DLY11/rotreg_inst/clk_gate_q_o_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_1
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_2
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_0
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_4
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_22
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_6
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_23
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_8
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_24
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_10
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_25
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_12
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_26
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_14
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_27
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_16
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_28
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_18
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_29
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_20
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_30
core/fu_TFG                         2703.0273      2.4     5.2224    31.1168      0.0000  fu_tfg_always_6_dataw32_busw32_nexpw4
core/fu_TFG/clk_gate_o1tmp_reg_0       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_3
core/fu_TFG/clk_gate_r1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_1
core/fu_TFG/clk_gate_r1reg_reg_1_0      1.6320     0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_0
core/fu_TFG/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_4
core/fu_TFG/fu_arch                 2660.1601      2.4    27.0912   155.5840      0.0000  tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_0
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_1
core/fu_TFG/fu_arch/u0                30.6816      0.0    30.6816     0.0000      0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_1
core/fu_TFG/fu_arch/u1                 3.1552      0.0     3.1552     0.0000      0.0000  radix_2_stagew3_nexpw4
core/fu_TFG/fu_arch/u2                60.4928      0.1    60.4928     0.0000      0.0000  weight_idxw16_stagew3_nexpw4_lut_addrw12
core/fu_TFG/fu_arch/u3                77.5744      0.1    77.5744     0.0000      0.0000  scale_k_idxw16_stagew3
core/fu_TFG/fu_arch/u4                25.5680      0.0    25.5680     0.0000      0.0000  lut_addr_idxw16_lut_addrw12
core/fu_TFG/fu_arch/u5              2106.5857      1.9     0.0000     0.0000      0.0000  tf_lut_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u5/lut          2106.5857      1.9  2072.6401    33.9456      0.0000  distr_2049x32_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u6               170.1632      0.2    73.9840    96.1792      0.0000  twiddle_twist_dataw32_halfw16
core/fu_add                          249.0432      0.2    27.5264    99.2256      0.0000  fu_add_sub_always_1_dataw32_busw32
core/fu_add/clk_gate_o1temp_reg_0      1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_0
core/fu_add/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_1
core/fu_add/fu_arch                  119.0272      0.1   119.0272     0.0000      0.0000  add_sub_arith_dataw32
core/fu_lsu_r                        177.9968      0.2    52.6592   118.8096      0.0000  fu_ldw_stw_always_4_dataw32_addrw16_1
core/fu_lsu_r/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_4
core/fu_lsu_r/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_3
core/fu_lsu_r/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_1
core/fu_lsu_r/clk_gate_r1_reg_reg_0      1.6320     0.0    0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_0
core/fu_lsu_w                        143.1808      0.1    36.4480   101.8368      0.0000  fu_ldw_stw_always_4_dataw32_addrw16_0
core/fu_lsu_w/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_4
core/fu_lsu_w/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_3
core/fu_lsu_w/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_1
core/fu_sh                           270.0416      0.2    27.6352    96.1792      0.0000  fu_shl_shr_always_1_dataw32_shiftw5
core/fu_sh/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_0
core/fu_sh/clk_gate_o1temp_reg_0       1.6320      0.0     0.0000     1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_1
core/fu_sh/fu_arch                   142.9632      0.1   142.9632     0.0000      0.0000  shl_shr_arith_dataw32_shiftw5
core/ic                             1307.4496      1.2    90.7392     0.0000      0.0000  tta0_interconn
core/ic/AG_i1                          7.8336      0.0     7.8336     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_13
core/ic/AG_i2                          1.9584      0.0     1.9584     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_1
core/ic/AG_i3                         13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_12
core/ic/AG_o1                        124.0320      0.1    27.8528    96.1792      0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_1
core/ic/CADD1_i1                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_11
core/ic/CADD1_i2                       0.4352      0.0     0.4352     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_01_DATAW1
core/ic/CADD1_o1                      16.9728      0.0    14.1440     2.8288      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_5
core/ic/CMUL_i1                       20.5632      0.0    20.5632     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_10
core/ic/CMUL_i2                       16.9728      0.0    16.9728     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_9
core/ic/CMUL_o1                      104.4480      0.1    13.9264    90.5216      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_4
core/ic/DLY10_i1                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_8
core/ic/DLY10_o1                     107.2768      0.1    13.9264    93.3504      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_3
core/ic/TFG_i1                         7.8336      0.0     7.8336     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_7
core/ic/TFG_i2                         1.7408      0.0     1.7408     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_0
core/ic/TFG_o1                       104.5568      0.1    14.0352    90.5216      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_2
core/ic/TFG_o2                         6.0928      0.0     0.4352     5.6576      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_2
core/ic/add_i1                        13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_6
core/ic/add_i2                        14.2528      0.0    14.2528     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_5
core/ic/add_o1                       132.6272      0.1    39.0592    93.5680      0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_1
core/ic/gcu_i1                         7.6160      0.0     4.7872     2.8288      0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_1
core/ic/gcu_i2                        27.8528      0.0     5.2224    22.6304      0.0000  tta0_output_socket_cons_2_1_BUSW_08_BUSW_18_DATAW_06
core/ic/gcu_o1                         8.3776      0.0     5.5488     2.8288      0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_0
core/ic/iter                          13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_4
core/ic/lsu_r_i1                       6.0928      0.0     6.0928     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_1
core/ic/lsu_r_i2                      13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_3
core/ic/lsu_r_o1                     107.2768      0.1    13.9264    93.3504      0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_1
core/ic/lsu_w_i1                       6.0928      0.0     6.0928     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_0
core/ic/lsu_w_i2                      14.2528      0.0    14.2528     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_2
core/ic/rf_def_i1                     13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_1
core/ic/rf_def_o1                     34.9248      0.0    29.2672     5.6576      0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_0
core/ic/sh_i1                          2.1760      0.0     2.1760     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW5
core/ic/sh_i2                         13.9264      0.0    13.9264     0.0000      0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_0
core/ic/sh_o1                        135.1296      0.1    38.9504    96.1792      0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_0
core/ic/simm_socket_B1                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_4
core/ic/simm_socket_B10                7.0720      0.0     1.4144     5.6576      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_0
core/ic/simm_socket_B2                13.0560      0.0     1.7408    11.3152      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_3
core/ic/simm_socket_B4                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_2
core/ic/simm_socket_B5                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_1
core/ic/simm_socket_B6                15.8848      0.0     1.7408    14.1440      0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_0
core/ic/simm_socket_B8                 4.2432      0.0     1.4144     2.8288      0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_1
core/inst_decoder                     92.0448      0.1    50.4832    28.5056      0.0000  tta0_decoder
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_7
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_6
core/inst_decoder/clk_gate_fu_gcu_opc_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_0
core/inst_decoder/clk_gate_simm_B1_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_5
core/inst_decoder/clk_gate_simm_B2_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_4
core/inst_decoder/clk_gate_simm_B4_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_3
core/inst_decoder/clk_gate_simm_B5_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_2
core/inst_decoder/clk_gate_simm_B6_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_1
core/inst_fetch                      261.1200      0.2   101.4016   149.9264      0.0000  tta0_ifetch
core/inst_fetch/clk_gate_instruction_reg_reg_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_0
core/inst_fetch/clk_gate_instruction_reg_reg_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_4
core/inst_fetch/clk_gate_instruction_reg_reg_1_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_5
core/inst_fetch/clk_gate_loop_iter_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_1
core/inst_fetch/clk_gate_loop_iter_temp_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_2
core/inst_fetch/clk_gate_return_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_3
core/loopbuffer                      974.7392      0.9   292.8896   670.4256      0.0000  tta0_loopbuf_1_32_51_1_0
core/loopbuffer/clk_gate_iter_cntr_r_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_5
core/loopbuffer/clk_gate_iter_cntr_r_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_7
core/loopbuffer/clk_gate_len_cntr_r_reg_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_6
core/loopbuffer/clk_gate_len_cntr_r_reg_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_8
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_1
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_4
core/loopbuffer/clk_gate_loop_len_r_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_3
core/rf_rf_def                       340.7616      0.3    49.9392   285.9264      0.0000  rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3
core/rf_rf_def/clk_gate_reg_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_1
core/rf_rf_def/clk_gate_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_2
core/rf_rf_def/clk_gate_reg_reg[2]_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_0
datamem/dmem_1                     47901.5384     43.3     0.4352     0.0000      0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_1
datamem/dmem_1/cacti_mem_0           262.8840      0.2     0.0000     0.0000    262.8840  cacti_mem_wrapper_DATAW32_ADDRW5_3
datamem/dmem_1/cacti_mem_x_1         262.8840      0.2     0.0000     0.0000    262.8840  cacti_mem_wrapper_DATAW32_ADDRW5_2
datamem/dmem_1/cacti_mem_x_2         460.4050      0.4     0.0000     0.0000    460.4050  cacti_mem_wrapper_DATAW32_ADDRW6_1
datamem/dmem_1/cacti_mem_x_3         811.6760      0.7     0.0000     0.0000    811.6760  cacti_mem_wrapper_DATAW32_ADDRW7_1
datamem/dmem_1/cacti_mem_x_4        1483.9500      1.3     0.0000     0.0000   1483.9500  cacti_mem_wrapper_DATAW32_ADDRW8_1
datamem/dmem_1/cacti_mem_x_5        3120.4099      2.8     0.0000     0.0000   3120.4099  cacti_mem_wrapper_DATAW32_ADDRW9_1
datamem/dmem_1/cacti_mem_x_6        5669.7402      5.1     0.0000     0.0000   5669.7402  cacti_mem_wrapper_DATAW32_ADDRW10_1
datamem/dmem_1/cacti_mem_x_7       11088.0996     10.0     0.0000     0.0000  11088.0996  cacti_mem_wrapper_DATAW32_ADDRW11_1
datamem/dmem_1/cacti_mem_x_8       24574.6992     22.2     0.0000     0.0000  24574.6992  cacti_mem_wrapper_DATAW32_ADDRW12_1
datamem/dmem_1/en_wr_arbiter          23.1744      0.0    23.1744     0.0000      0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_1
datamem/dmem_1/mem_output_sel        143.1808      0.1   117.7216    25.4592      0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_1
datamem/dmem_2                     47900.2328     43.3     0.0000     0.0000      0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_0
datamem/dmem_2/cacti_mem_0           262.8840      0.2     0.0000     0.0000    262.8840  cacti_mem_wrapper_DATAW32_ADDRW5_1
datamem/dmem_2/cacti_mem_x_1         262.8840      0.2     0.0000     0.0000    262.8840  cacti_mem_wrapper_DATAW32_ADDRW5_0
datamem/dmem_2/cacti_mem_x_2         460.4050      0.4     0.0000     0.0000    460.4050  cacti_mem_wrapper_DATAW32_ADDRW6_0
datamem/dmem_2/cacti_mem_x_3         811.6760      0.7     0.0000     0.0000    811.6760  cacti_mem_wrapper_DATAW32_ADDRW7_0
datamem/dmem_2/cacti_mem_x_4        1483.9500      1.3     0.0000     0.0000   1483.9500  cacti_mem_wrapper_DATAW32_ADDRW8_0
datamem/dmem_2/cacti_mem_x_5        3120.4099      2.8     0.0000     0.0000   3120.4099  cacti_mem_wrapper_DATAW32_ADDRW9_0
datamem/dmem_2/cacti_mem_x_6        5669.7402      5.1     0.0000     0.0000   5669.7402  cacti_mem_wrapper_DATAW32_ADDRW10_0
datamem/dmem_2/cacti_mem_x_7       11088.0996     10.0     0.0000     0.0000  11088.0996  cacti_mem_wrapper_DATAW32_ADDRW11_0
datamem/dmem_2/cacti_mem_x_8       24574.6992     22.2     0.0000     0.0000  24574.6992  cacti_mem_wrapper_DATAW32_ADDRW12_0
datamem/dmem_2/en_wr_arbiter          22.5216      0.0    22.5216     0.0000      0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_0
datamem/dmem_2/mem_output_sel        142.9632      0.1   117.5040    25.4592      0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_0
datamem/par_mem_logic_inst           811.3216      0.7     1.7408     0.0000      0.0000  par_mem_logic_PORTS2_LSU_ADDRW14_CTRLW1_DATAW32_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0    165.0496     0.1   78.5536   84.8640     0.0000 addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0/clk_gate_addrs_latch_reg[0]      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/ctrl_units_0     70.3936     0.1    9.4656    5.6576     0.0000 ctrl_units_PORTS2_CTRLW1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0     29.0496     0.0   10.4448   16.9728     0.0000 ctrl_unit_PORTS2_CTRLW1_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1     26.2208     0.0   10.4448   14.1440     0.0000 ctrl_unit_PORTS2_CTRLW1_0
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_0
datamem/par_mem_logic_inst/rd_crsbar_0    293.5424     0.3  103.5776  186.7008     0.0000 rd_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[1]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0    280.5952     0.3   96.2880  181.0432     0.0000 wr_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/scheduler                    686.3104      0.6    85.5168   260.2496      0.0000  par_mem_scheduler_dataw32_addrw14_ports2_rd_delay1
datamem/scheduler/addr_a_conv         16.9728      0.0    12.5120     2.8288      0.0000  ser_par_conv_portw14_port_cnt2_1
datamem/scheduler/addr_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_1
datamem/scheduler/addr_b_conv         52.1152      0.0     8.0512    42.4320      0.0000  ser_par_conv_portw14_port_cnt2_0
datamem/scheduler/addr_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_0
datamem/scheduler/d_a_conv            27.3088      0.0    22.8480     2.8288      0.0000  ser_par_conv_portw32_port_cnt2_3
datamem/scheduler/d_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_1
datamem/scheduler/d_b_conv           113.9136      0.1    15.8848    96.3968      0.0000  ser_par_conv_portw32_port_cnt2_1
datamem/scheduler/d_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_0
datamem/scheduler/par_ser_conv_inst    130.2336     0.1   27.9616    99.0080      0.0000  par_ser_conv_portw32_port_cnt2
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_1
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0_0      1.6320     0.0    0.0000    1.6320     0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_2
instmem/cacti_mem                    708.8940      0.6     0.0000     0.0000    708.8940  cacti_mem_wrapper_DATAW51_ADDRW6
--------------------------------  -----------  -------  ---------  ---------  ----------  -----------------------------------------------------------------------------------
Total                                                   8140.0898  6272.5375  96178.3899

1
