 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 23:25:01 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              20.00
  Critical Path Length:          3.07
  Critical Path Slack:           0.19
  Critical Path Clk Period:      3.33
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        212
  Hierarchical Port Count:      11253
  Leaf Cell Count:              20021
  Buf/Inv Cell Count:            3953
  Buf Cell Count:                 949
  Inv Cell Count:                3004
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     17653
  Sequential Cell Count:         2368
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     9847.379493
  Noncombinational Area:  6463.699123
  Buf/Inv Area:           1242.496024
  Total Buffer Area:           462.94
  Total Inverter Area:         779.55
  Macro/Black Box Area: 110462.496521
  Net Area:                  0.000000
  Net XLength        :      102156.91
  Net YLength        :      112755.17
  -----------------------------------
  Cell Area:            126773.575137
  Design Area:          126773.575137
  Net Length        :       214912.08


  Design Rules
  -----------------------------------
  Total Number of Nets:         21567
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  354.01
  Logic Optimization:                 33.92
  Mapping Optimization:              310.17
  -----------------------------------------
  Overall Compile Time:              889.19
  Overall Compile Wall Clock Time:   661.04

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
