 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 08:15:33 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              35.00
  Critical Path Length:          1.20
  Critical Path Slack:           0.02
  Critical Path Clk Period:      1.25
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11260
  Leaf Cell Count:              17342
  Buf/Inv Cell Count:            3126
  Buf Cell Count:                 878
  Inv Cell Count:                2248
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     15036
  Sequential Cell Count:         2306
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8159.673843
  Noncombinational Area:  6280.588720
  Buf/Inv Area:            917.401614
  Total Buffer Area:           410.07
  Total Inverter Area:         507.33
  Macro/Black Box Area:  90431.190369
  Net Area:                  0.000000
  Net XLength        :      101429.46
  Net YLength        :      101275.30
  -----------------------------------
  Cell Area:            104871.452932
  Design Area:          104871.452932
  Net Length        :       202704.75


  Design Rules
  -----------------------------------
  Total Number of Nets:         19107
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  247.74
  Logic Optimization:                 24.07
  Mapping Optimization:              111.62
  -----------------------------------------
  Overall Compile Time:              573.24
  Overall Compile Wall Clock Time:   484.10

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
