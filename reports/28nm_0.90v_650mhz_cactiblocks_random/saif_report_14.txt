Warning: There are 100 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 18:17:11 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         279(66.43%)       0(0.00%)          141(33.57%)        420
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         249(39.90%)       0(0.00%)          375(60.10%)        624
--------------------------------------------------------------------------------
1
