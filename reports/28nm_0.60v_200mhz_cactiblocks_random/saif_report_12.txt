Warning: There are 102 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 22:43:51 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         287(65.23%)       0(0.00%)          153(34.77%)        440
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         228(32.52%)       0(0.00%)          473(67.48%)        701
--------------------------------------------------------------------------------
1
