 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 08:51:44 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2170 (99.09%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    20 (0.91%)    |
          |                                       |                  |
          |    Total number of registers          |     2190         |
          ------------------------------------------------------------



1
