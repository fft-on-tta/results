 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 20:41:45 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              28.00
  Critical Path Length:          0.47
  Critical Path Slack:           0.01
  Critical Path Clk Period:      1.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11232
  Leaf Cell Count:              20065
  Buf/Inv Cell Count:            3913
  Buf Cell Count:                 983
  Inv Cell Count:                2930
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     17734
  Sequential Cell Count:         2331
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:    10171.059485
  Noncombinational Area:  6357.183922
  Buf/Inv Area:           1297.440024
  Total Buffer Area:           499.61
  Total Inverter Area:         797.83
  Macro/Black Box Area:  96178.389893
  Net Area:                  0.000000
  Net XLength        :      115850.34
  Net YLength        :      106046.55
  -----------------------------------
  Cell Area:            112706.633300
  Design Area:          112706.633300
  Net Length        :       221896.88


  Design Rules
  -----------------------------------
  Total Number of Nets:         21657
  Nets With Violations:             1
  Max Trans Violations:             0
  Max Cap Violations:               1
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  351.16
  Logic Optimization:                 38.55
  Mapping Optimization:              314.22
  -----------------------------------------
  Overall Compile Time:              894.36
  Overall Compile Wall Clock Time:   657.29

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
