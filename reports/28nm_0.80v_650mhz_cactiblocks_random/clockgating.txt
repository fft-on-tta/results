 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 05:32:32 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2187 (99.23%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.77%)    |
          |                                       |                  |
          |    Total number of registers          |     2204         |
          ------------------------------------------------------------



1
