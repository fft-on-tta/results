Information: Propagating switching activity (high effort zero delay simulation). (PWR-6)
Warning: Design has unannotated sequential cell outputs. (PWR-415)
Warning: Design has unannotated black box outputs. (PWR-428)
 
****************************************
Report : power
        -analysis_effort high
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 23:24:00 2018
****************************************


Library(s) Used:

    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP4_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP10_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CORE_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs/C28SOI_SC_8_CORE_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.95V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x51/ram_64x51.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_95v_32x32/ram_32x32.db)


Operating Conditions: tt28_0.95V_0.00V_0.00V_0.00V_25C   Library: C28SOI_SC_8_CLK_LL
Wire Load Model Mode: Inactive.


Global Operating Voltage = 0.95 
Power-specific unit information :
    Voltage Units = 1V
    Capacitance Units = 1.000000pf
    Time Units = 1ns
    Dynamic Power Units = 1mW    (derived from V,C,T units)
    Leakage Power Units = 1mW

Warning: Cannot report correlated power unless power prediction mode is set. (PWR-727)
Power Breakdown
---------------

                             Cell        Driven Net  Tot Dynamic      Cell
                             Internal    Switching   Power (mW)       Leakage
Cell                         Power (mW)  Power (mW)  (% Cell/Tot)     Power (mW)
--------------------------------------------------------------------------------
Netlist Power                   3.0185      2.0474      5.0659 (60%)    19.2667
Estimated Clock Tree Power   N/A         N/A          (N/A)           N/A
--------------------------------------------------------------------------------

                 Internal         Switching           Leakage            Total
Power Group      Power            Power               Power              Power   (   %    )  Attrs
--------------------------------------------------------------------------------------------------
io_pad             0.0000            0.0000            0.0000            0.0000  (   0.00%)
memory             0.2209        1.4449e-02           19.2619           19.4973  (  80.13%)
black_box          0.0000            0.0000            0.0000            0.0000  (   0.00%)
clock_network  7.5555e-02            0.3722        3.7839e-04            0.4481  (   1.84%)
register           1.7856        7.6255e-02        2.8534e-03            1.8647  (   7.66%)
sequential         0.0000            0.0000            0.0000            0.0000  (   0.00%)
combinational      0.9364            1.5845        1.5778e-03            2.5224  (  10.37%)
--------------------------------------------------------------------------------------------------
Total              3.0185 mW         2.0474 mW        19.2667 mW        24.3326 mW
1
