 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 07:39:27 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |       99         |
          |                                       |                  |
          |    Number of Gated registers          |  2138 (99.21%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.79%)    |
          |                                       |                  |
          |    Total number of registers          |     2155         |
          ------------------------------------------------------------



1
