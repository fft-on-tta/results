 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 03:34:11 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2193 (98.96%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    23 (1.04%)    |
          |                                       |                  |
          |    Total number of registers          |     2216         |
          ------------------------------------------------------------



1
