 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 13:43:07 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              32.00
  Critical Path Length:          0.53
  Critical Path Slack:           0.01
  Critical Path Clk Period:      1.05
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        212
  Hierarchical Port Count:      11245
  Leaf Cell Count:              18691
  Buf/Inv Cell Count:            3548
  Buf Cell Count:                 961
  Inv Cell Count:                2587
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16357
  Sequential Cell Count:         2334
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     9881.433893
  Noncombinational Area:  6364.038322
  Buf/Inv Area:           1118.790420
  Total Buffer Area:           451.30
  Total Inverter Area:         667.49
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :      108057.78
  Net YLength        :      106335.52
  -----------------------------------
  Cell Area:            112414.411667
  Design Area:          112414.411667
  Net Length        :       214393.30


  Design Rules
  -----------------------------------
  Total Number of Nets:         20587
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  388.22
  Logic Optimization:                 38.12
  Mapping Optimization:              155.77
  -----------------------------------------
  Overall Compile Time:              782.32
  Overall Compile Wall Clock Time:   653.45

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
