Warning: There are 90 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 00:35:54 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         274(65.39%)       0(0.00%)          145(34.61%)        419
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         215(32.28%)       0(0.00%)          451(67.72%)        666
--------------------------------------------------------------------------------
1
