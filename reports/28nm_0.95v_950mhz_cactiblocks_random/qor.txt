 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 02:53:16 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              34.00
  Critical Path Length:          0.50
  Critical Path Slack:           0.04
  Critical Path Clk Period:      1.05
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11228
  Leaf Cell Count:              19139
  Buf/Inv Cell Count:            3676
  Buf Cell Count:                 975
  Inv Cell Count:                2701
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16819
  Sequential Cell Count:         2320
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     9402.061081
  Noncombinational Area:  6324.108722
  Buf/Inv Area:           1145.772821
  Total Buffer Area:           471.76
  Total Inverter Area:         674.02
  Macro/Black Box Area:  96186.932556
  Net Area:                  0.000000
  Net XLength        :      113773.76
  Net YLength        :      119731.42
  -----------------------------------
  Cell Area:            111913.102359
  Design Area:          111913.102359
  Net Length        :       233505.19


  Design Rules
  -----------------------------------
  Total Number of Nets:         20787
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  337.95
  Logic Optimization:                 32.89
  Mapping Optimization:              234.02
  -----------------------------------------
  Overall Compile Time:              797.91
  Overall Compile Wall Clock Time:   617.82

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
