 
****************************************
Report : area
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 23:03:56 2018
****************************************

Library(s) Used:

    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP4_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_COREPBP10_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_64x51/ram_64x51.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_32x32/ram_32x32.db)
    C28SOI_SC_8_CORE_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs/C28SOI_SC_8_CORE_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db)

Number of ports:                        11457
Number of nets:                         31324
Number of cells:                        20639
Number of combinational cells:          17692
Number of sequential cells:              2718
Number of macros/black boxes:              19
Number of buf/inv:                       4037
Number of references:                      49

Combinational area:               9669.382686
Buf/Inv area:                     1239.667223
Noncombinational area:            6346.630321
Macro/Black Box area:           110462.496521
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                126478.509527
Total area:                 undefined

Hierarchical area distribution
------------------------------

                                  Global cell area               Local cell area
                                  --------------------  --------------------------------- 
Hierarchical cell                 Absolute     Percent  Combi-     Noncombi-  Black-
                                  Total        Total    national   national   boxes        Design
--------------------------------  -----------  -------  ---------  ---------  -----------  -----------------------------------------------------------------------------------
proc                              126478.5095    100.0    85.8432   141.6576       0.0000  proc
clk_gate_cnt_rd_reg                    1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0_4_0
clk_gate_en_blocks_reg_reg             1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_block_mem_output_sel_DATAW32_NBLOCKS9_1_2
clk_gate_tf_reg                        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16_0
core/decomp                           22.5216      0.0    22.5216     0.0000       0.0000  tta0_decompressor
core/fu_AG                           616.2432      0.5    33.4016   115.9808       0.0000  fu_ag_always_2_dataw32_busw32_nexpw4
core/fu_AG/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_5
core/fu_AG/clk_gate_o1tmp_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_3
core/fu_AG/clk_gate_o2reg_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_4
core/fu_AG/clk_gate_o2tmp_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_2
core/fu_AG/clk_gate_r1reg_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_1
core/fu_AG/clk_gate_t1reg_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ag_always_2_dataw32_busw32_nexpw4_0
core/fu_AG/fu_arch                   457.0688      0.4   251.9808    96.1792       0.0000  ag_arith_dataw32_nexpw4_idxw16_stagew3_mau_shift2
core/fu_AG/fu_arch/u0                108.9088      0.1    49.5040    59.4048       0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_0
core/fu_CADD1                       1348.5760      1.1     0.4352     0.0000       0.0000  fu_cadd_always_1_dataw32_busw32
core/fu_CADD1/fu_arch               1348.1408      1.1     0.0000     0.0000       0.0000  cadd_full_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_arith    993.5616     0.8  678.9120  314.6496      0.0000  cadd_arith_dataw32_halfw16
core/fu_CADD1/fu_arch/inst_cadd_reg    354.5792     0.3   59.8400   280.0512       0.0000  cadd_reg_dataw32
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_8
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_a_reg_reg_0_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_9
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_4
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_0
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_1
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_2      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_2
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_0_3      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_3
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_b_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_7
core/fu_CADD1/fu_arch/inst_cadd_reg/clk_gate_c_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cadd_reg_dataw32_6
core/fu_CMUL                        3726.9441      2.9   125.2288   195.1872       0.0000  fu_cmul_always_3_dataw32_busw32
core/fu_CMUL/clk_gate_o1reg_reg_0      1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_0
core/fu_CMUL/clk_gate_o1reg_reg_0_0      1.6320     0.0    0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_3
core/fu_CMUL/clk_gate_o1tmp_reg_0      1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_2
core/fu_CMUL/clk_gate_r1reg_reg_0      1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_cmul_always_3_dataw32_busw32_1
core/fu_CMUL/fu_arch                3400.0001      2.7  3216.0193   182.3488       0.0000  cmul_pipe_1_dataw32_halfw16
core/fu_CMUL/fu_arch/clk_gate_out_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_cmul_pipe_1_dataw32_halfw16
core/fu_DLY11                       1333.4528      1.1     0.6528   107.7120       0.0000  fu_rotreg_always_11_dataw32_busw32
core/fu_DLY11/clk_gate_cnt_wr_reg      1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_0
core/fu_DLY11/clk_gate_r1reg_reg_0      1.6320     0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_rotreg_always_11_dataw32_busw32_1
core/fu_DLY11/rotreg_inst           1221.8240      1.0   180.4992  1007.0528       0.0000  rotreg_dataw32_reg_count10
core/fu_DLY11/rotreg_inst/clk_gate_q_o_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_1
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_2
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[0]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_0
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_4
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[1]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_22
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_6
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[2]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_23
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_8
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[3]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_24
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_10
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[4]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_25
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_12
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[5]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_26
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_14
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[6]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_27
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_16
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[7]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_28
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_18
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[8]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_29
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_20
core/fu_DLY11/rotreg_inst/clk_gate_reg_reg[9]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rotreg_dataw32_reg_count10_30
core/fu_TFG                         2871.4497      2.3     8.3776    31.1168       0.0000  fu_tfg_always_6_dataw32_busw32_nexpw4
core/fu_TFG/clk_gate_o1tmp_reg_0       1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_3
core/fu_TFG/clk_gate_r1reg_reg_1       1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_1
core/fu_TFG/clk_gate_r1reg_reg_1_0      1.6320     0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_0
core/fu_TFG/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_tfg_always_6_dataw32_busw32_nexpw4_4
core/fu_TFG/fu_arch                 2825.4273      2.2    30.0288   155.5840       0.0000  tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_0
core/fu_TFG/fu_arch/clk_gate_opc_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tfg_pipe_4_dataw32_halfw16_nexpw4_idxw16_lut_addrw12_stagew3_1
core/fu_TFG/fu_arch/u0                39.4944      0.0    39.4944     0.0000       0.0000  idx_stage_split_dataw32_nexpw4_stagew3_idxw16_1
core/fu_TFG/fu_arch/u1                 3.1552      0.0     3.1552     0.0000       0.0000  radix_2_stagew3_nexpw4
core/fu_TFG/fu_arch/u2                69.6320      0.1    69.6320     0.0000       0.0000  weight_idxw16_stagew3_nexpw4_lut_addrw12
core/fu_TFG/fu_arch/u3                97.8112      0.1    97.8112     0.0000       0.0000  scale_k_idxw16_stagew3
core/fu_TFG/fu_arch/u4                28.1792      0.0    28.1792     0.0000       0.0000  lut_addr_idxw16_lut_addrw12
core/fu_TFG/fu_arch/u5              2228.1153      1.8     0.4352     0.0000       0.0000  tf_lut_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u5/lut          2227.6801      1.8  2190.4705    37.2096       0.0000  distr_2049x32_synch_dataw32_lut_addrw12
core/fu_TFG/fu_arch/u6               170.1632      0.1    73.9840    96.1792       0.0000  twiddle_twist_dataw32_halfw16
core/fu_add                          259.7056      0.2    27.5264    99.0080       0.0000  fu_add_sub_always_1_dataw32_busw32
core/fu_add/clk_gate_o1temp_reg_0      1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_0
core/fu_add/clk_gate_t1reg_reg_1       1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_add_sub_always_1_dataw32_busw32_1
core/fu_add/fu_arch                  129.9072      0.1   129.9072     0.0000       0.0000  add_sub_arith_dataw32
core/fu_lsu_r                        177.8880      0.1    52.5504   118.8096       0.0000  fu_ldw_stw_always_4_dataw32_addrw16_1
core/fu_lsu_r/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_4
core/fu_lsu_r/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_3
core/fu_lsu_r/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_1
core/fu_lsu_r/clk_gate_r1_reg_reg_0      1.6320     0.0    0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_1_0
core/fu_lsu_w                        144.3776      0.1    37.6448   101.8368       0.0000  fu_ldw_stw_always_4_dataw32_addrw16_0
core/fu_lsu_w/clk_gate_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000  SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_4
core/fu_lsu_w/clk_gate_data_out_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_3
core/fu_lsu_w/clk_gate_o1shadow_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_fu_ldw_stw_always_4_dataw32_addrw16_0_1
core/fu_sh                           272.7616      0.2    27.8528    96.1792       0.0000  fu_shl_shr_always_1_dataw32_shiftw5
core/fu_sh/clk_gate_o1reg_reg_0        1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_0
core/fu_sh/clk_gate_o1temp_reg_0       1.6320      0.0     0.0000     1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_fu_shl_shr_always_1_dataw32_shiftw5_1
core/fu_sh/fu_arch                   145.4656      0.1   145.4656     0.0000       0.0000  shl_shr_arith_dataw32_shiftw5
core/ic                             1380.0192      1.1   145.4656     0.0000       0.0000  tta0_interconn
core/ic/AG_i1                          7.8336      0.0     7.8336     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_13
core/ic/AG_i2                          1.7408      0.0     1.7408     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_1
core/ic/AG_i3                         14.1440      0.0    14.1440     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_12
core/ic/AG_o1                        124.2496      0.1    27.8528    96.3968       0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_1
core/ic/CADD1_i1                      13.9264      0.0    13.9264     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_11
core/ic/CADD1_i2                       0.4352      0.0     0.4352     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_01_DATAW1
core/ic/CADD1_o1                      18.2784      0.0    15.4496     2.8288       0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_5
core/ic/CMUL_i1                       13.4912      0.0    13.4912     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_10
core/ic/CMUL_i2                       13.4912      0.0    13.4912     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_9
core/ic/CMUL_o1                      104.4480      0.1    13.9264    90.5216       0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_4
core/ic/DLY10_i1                      14.2528      0.0    14.2528     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_8
core/ic/DLY10_o1                     107.2768      0.1    13.9264    93.3504       0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_3
core/ic/TFG_i1                         7.8336      0.0     7.8336     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_7
core/ic/TFG_i2                         2.2848      0.0     2.2848     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW4_0
core/ic/TFG_o1                       104.9920      0.1    14.4704    90.5216       0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_2
core/ic/TFG_o2                         6.0928      0.0     0.4352     5.6576       0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_2
core/ic/add_i1                        13.9264      0.0    13.9264     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_6
core/ic/add_i2                        14.1440      0.0    14.1440     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_5
core/ic/add_o1                       136.6528      0.1    43.3024    93.3504       0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_1
core/ic/gcu_i1                         7.6160      0.0     4.7872     2.8288       0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_1
core/ic/gcu_i2                        27.8528      0.0     5.2224    22.6304       0.0000  tta0_output_socket_cons_2_1_BUSW_08_BUSW_18_DATAW_06
core/ic/gcu_o1                         8.3776      0.0     5.5488     2.8288       0.0000  tta0_input_socket_cons_2_BUSW_032_BUSW_132_DATAW6_0
core/ic/iter                          13.9264      0.0    13.9264     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_4
core/ic/lsu_r_i1                       6.0928      0.0     6.0928     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_1
core/ic/lsu_r_i2                      13.9264      0.0    13.9264     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_3
core/ic/lsu_r_o1                     107.8208      0.1    14.2528    93.5680       0.0000  tta0_output_socket_cons_1_1_BUSW_032_DATAW_032_1
core/ic/lsu_w_i1                       6.0928      0.0     6.0928     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW16_0
core/ic/lsu_w_i2                      13.8176      0.0    13.8176     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_2
core/ic/rf_def_i1                     13.9264      0.0    13.9264     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_1
core/ic/rf_def_o1                     56.0320      0.0    50.1568     5.8752       0.0000  tta0_output_socket_cons_2_1_BUSW_032_BUSW_132_DATAW_032_0
core/ic/sh_i1                          2.1760      0.0     2.1760     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW5
core/ic/sh_i2                         14.1440      0.0    14.1440     0.0000       0.0000  tta0_input_socket_cons_1_BUSW_032_DATAW32_0
core/ic/sh_o1                        135.2384      0.1    39.0592    96.1792       0.0000  tta0_output_socket_cons_3_1_BUSW_032_BUSW_132_BUSW_232_DATAW_032_0
core/ic/simm_socket_B1                15.8848      0.0     1.7408    14.1440       0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_4
core/ic/simm_socket_B10                7.0720      0.0     1.4144     5.6576       0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_0
core/ic/simm_socket_B2                13.0560      0.0     1.7408    11.3152       0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_3
core/ic/simm_socket_B4                15.8848      0.0     1.7408    14.1440       0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_2
core/ic/simm_socket_B5                15.8848      0.0     1.7408    14.1440       0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_1
core/ic/simm_socket_B6                15.9936      0.0     1.8496    14.1440       0.0000  tta0_output_socket_cons_1_1_BUSW_04_DATAW_04_0
core/ic/simm_socket_B8                 4.2432      0.0     1.4144     2.8288       0.0000  tta0_output_socket_cons_1_1_BUSW_01_DATAW_01_1
core/inst_decoder                     93.0240      0.1    54.5088    25.4592       0.0000  tta0_decoder
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_7
core/inst_decoder/clk_gate_fu_TFG_t_load_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_6
core/inst_decoder/clk_gate_fu_gcu_opc_reg_reg      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_0
core/inst_decoder/clk_gate_simm_B1_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_5
core/inst_decoder/clk_gate_simm_B2_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_4
core/inst_decoder/clk_gate_simm_B4_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_3
core/inst_decoder/clk_gate_simm_B5_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_2
core/inst_decoder/clk_gate_simm_B6_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_decoder_1
core/inst_fetch                      263.6224      0.2   103.9040   149.9264       0.0000  tta0_ifetch
core/inst_fetch/clk_gate_instruction_reg_reg_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_0
core/inst_fetch/clk_gate_instruction_reg_reg_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_4
core/inst_fetch/clk_gate_instruction_reg_reg_1_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_5
core/inst_fetch/clk_gate_loop_iter_reg_reg      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_1
core/inst_fetch/clk_gate_loop_iter_temp_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_2
core/inst_fetch/clk_gate_return_addr_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_ifetch_3
core/loopbuffer                     1032.0768      0.8   354.4704   667.8144       0.0000  tta0_loopbuf_1_32_51_1_0
core/loopbuffer/clk_gate_iter_cntr_r_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_5
core/loopbuffer/clk_gate_len_cntr_r_reg_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_6
core/loopbuffer/clk_gate_len_cntr_r_reg_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_7
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_1
core/loopbuffer/clk_gate_loop_buffer_r_reg[0]_1_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_4
core/loopbuffer/clk_gate_loop_len_r_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_tta0_loopbuf_1_32_51_1_0_3
core/rf_rf_def                       385.5872      0.3    89.1072   291.5840       0.0000  rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3
core/rf_rf_def/clk_gate_reg_reg[0]_0      1.6320     0.0    0.0000    1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_1
core/rf_rf_def/clk_gate_reg_reg[1]_0      1.6320     0.0    0.0000    1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_2
core/rf_rf_def/clk_gate_reg_reg[2]_0      1.6320     0.0    0.0000    1.6320       0.0000  SNPS_CLOCK_GATE_HIGH_rf_1wr_1rd_always_1_guarded_0_dataw32_rf_size3_0
datamem/dmem_1                     55044.3155     43.5     0.0000     0.0000       0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_1
datamem/dmem_1/cacti_mem_0           262.7680      0.2     0.0000     0.0000     262.7680  cacti_mem_wrapper_DATAW32_ADDRW5_3
datamem/dmem_1/cacti_mem_x_1         262.7680      0.2     0.0000     0.0000     262.7680  cacti_mem_wrapper_DATAW32_ADDRW5_2
datamem/dmem_1/cacti_mem_x_2         460.2890      0.4     0.0000     0.0000     460.2890  cacti_mem_wrapper_DATAW32_ADDRW6_1
datamem/dmem_1/cacti_mem_x_3         811.5570      0.6     0.0000     0.0000     811.5570  cacti_mem_wrapper_DATAW32_ADDRW7_1
datamem/dmem_1/cacti_mem_x_4        1483.7300      1.2     0.0000     0.0000    1483.7300  cacti_mem_wrapper_DATAW32_ADDRW8_1
datamem/dmem_1/cacti_mem_x_5        3119.9399      2.5     0.0000     0.0000    3119.9399  cacti_mem_wrapper_DATAW32_ADDRW9_1
datamem/dmem_1/cacti_mem_x_6        6128.6201      4.8     0.0000     0.0000    6128.6201  cacti_mem_wrapper_DATAW32_ADDRW10_1
datamem/dmem_1/cacti_mem_x_7       13619.2002     10.8     0.0000     0.0000   13619.2002  cacti_mem_wrapper_DATAW32_ADDRW11_1
datamem/dmem_1/cacti_mem_x_8       28728.0000     22.7     0.0000     0.0000   28728.0000  cacti_mem_wrapper_DATAW32_ADDRW12_1
datamem/dmem_1/en_wr_arbiter          21.7600      0.0    21.7600     0.0000       0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_1
datamem/dmem_1/mem_output_sel        145.6832      0.1   120.2240    25.4592       0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_1
datamem/dmem_2                     55036.4819     43.5     0.0000     0.0000       0.0000  synch_singleport_mem_wrapper_DATAW32_ADDRW13_0
datamem/dmem_2/cacti_mem_0           262.7680      0.2     0.0000     0.0000     262.7680  cacti_mem_wrapper_DATAW32_ADDRW5_1
datamem/dmem_2/cacti_mem_x_1         262.7680      0.2     0.0000     0.0000     262.7680  cacti_mem_wrapper_DATAW32_ADDRW5_0
datamem/dmem_2/cacti_mem_x_2         460.2890      0.4     0.0000     0.0000     460.2890  cacti_mem_wrapper_DATAW32_ADDRW6_0
datamem/dmem_2/cacti_mem_x_3         811.5570      0.6     0.0000     0.0000     811.5570  cacti_mem_wrapper_DATAW32_ADDRW7_0
datamem/dmem_2/cacti_mem_x_4        1483.7300      1.2     0.0000     0.0000    1483.7300  cacti_mem_wrapper_DATAW32_ADDRW8_0
datamem/dmem_2/cacti_mem_x_5        3119.9399      2.5     0.0000     0.0000    3119.9399  cacti_mem_wrapper_DATAW32_ADDRW9_0
datamem/dmem_2/cacti_mem_x_6        6128.6201      4.8     0.0000     0.0000    6128.6201  cacti_mem_wrapper_DATAW32_ADDRW10_0
datamem/dmem_2/cacti_mem_x_7       13619.2002     10.8     0.0000     0.0000   13619.2002  cacti_mem_wrapper_DATAW32_ADDRW11_0
datamem/dmem_2/cacti_mem_x_8       28728.0000     22.7     0.0000     0.0000   28728.0000  cacti_mem_wrapper_DATAW32_ADDRW12_0
datamem/dmem_2/en_wr_arbiter          14.2528      0.0    14.2528     0.0000       0.0000  block_en_wr_arbiter_ADDRW13_NBLOCKS9_0
datamem/dmem_2/mem_output_sel        145.3568      0.1   119.8976    25.4592       0.0000  block_mem_output_sel_DATAW32_NBLOCKS9_0
datamem/par_mem_logic_inst           839.7184      0.7     1.3056     0.0000       0.0000  par_mem_logic_PORTS2_LSU_ADDRW14_CTRLW1_DATAW32_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0    176.9088     0.1   90.4128   84.8640      0.0000 addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/addr_crsbar_0/clk_gate_addrs_latch_reg[0]      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_addr_crsbar_PORTS2_LSU_ADDRW14_CTRLW1_PM_FUNC2
datamem/par_mem_logic_inst/ctrl_units_0     68.8704     0.1    8.0512    5.6576      0.0000 ctrl_units_PORTS2_CTRLW1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0     28.3968     0.0   12.6208   14.1440      0.0000 ctrl_unit_PORTS2_CTRLW1_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_0/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_1
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1     26.7648     0.0   10.9888   14.1440      0.0000 ctrl_unit_PORTS2_CTRLW1_0
datamem/par_mem_logic_inst/ctrl_units_0/ctrl_uniti_1/clk_gate_rd_mux_ctrl_latch_reg_reg      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ctrl_unit_PORTS2_CTRLW1_0_0
datamem/par_mem_logic_inst/rd_crsbar_0    294.7392     0.2  104.7744  186.7008      0.0000 rd_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/par_mem_logic_inst/rd_crsbar_0/clk_gate_crsbar_out_latch_reg[1]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_rd_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0    297.8944     0.2  113.5872  181.0432      0.0000 wr_crsbar_PORTS2_DATAW32_CTRLW1
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_0
datamem/par_mem_logic_inst/wr_crsbar_0/clk_gate_st_data_latch_reg[0]_0_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_wr_crsbar_PORTS2_DATAW32_CTRLW1_1
datamem/scheduler                    688.5952      0.5    86.9312   260.2496       0.0000  par_mem_scheduler_dataw32_addrw14_ports2_rd_delay1
datamem/scheduler/addr_a_conv         17.8432      0.0    13.3824     2.8288       0.0000  ser_par_conv_portw14_port_cnt2_1
datamem/scheduler/addr_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_1
datamem/scheduler/addr_b_conv         52.1152      0.0     8.0512    42.4320       0.0000  ser_par_conv_portw14_port_cnt2_0
datamem/scheduler/addr_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw14_port_cnt2_0_0
datamem/scheduler/d_a_conv            27.4176      0.0    22.9568     2.8288       0.0000  ser_par_conv_portw32_port_cnt2_3
datamem/scheduler/d_a_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_1
datamem/scheduler/d_b_conv           113.9136      0.1    15.8848    96.3968       0.0000  ser_par_conv_portw32_port_cnt2_1
datamem/scheduler/d_b_conv/clk_gate_inp_regs_reg[0]_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_ser_par_conv_portw32_port_cnt2_1_0
datamem/scheduler/par_ser_conv_inst    130.1248     0.1   27.8528    99.0080       0.0000  par_ser_conv_portw32_port_cnt2
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_1
datamem/scheduler/par_ser_conv_inst/clk_gate_inp_reg_reg_0_0      1.6320     0.0    0.0000    1.6320      0.0000 SNPS_CLOCK_GATE_HIGH_par_ser_conv_portw32_port_cnt2_2
instmem/cacti_mem                    708.7520      0.6     0.0000     0.0000     708.7520  cacti_mem_wrapper_DATAW51_ADDRW6
--------------------------------  -----------  -------  ---------  ---------  -----------  -----------------------------------------------------------------------------------
Total                                                   9669.3827  6346.6303  110462.4965

1
