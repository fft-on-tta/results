 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Tue Jul 31 10:36:30 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              54.00
  Critical Path Length:         11.43
  Critical Path Slack:           8.54
  Critical Path Clk Period:     20.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        210
  Hierarchical Port Count:      11250
  Leaf Cell Count:              15368
  Buf/Inv Cell Count:            2567
  Buf Cell Count:                1076
  Inv Cell Count:                1491
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     13095
  Sequential Cell Count:         2273
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:    31641.480442
  Noncombinational Area: 17707.320126
  Buf/Inv Area:           3251.160127
  Total Buffer Area:          1623.96
  Total Inverter Area:        1627.20
  Macro/Black Box Area: 487756.227051
  Net Area:                  0.000000
  Net XLength        :      184455.23
  Net YLength        :      205444.42
  -----------------------------------
  Cell Area:            537105.027618
  Design Area:          537105.027618
  Net Length        :       389899.66


  Design Rules
  -----------------------------------
  Total Number of Nets:         17134
  Nets With Violations:            13
  Max Trans Violations:             8
  Max Cap Violations:               7
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  239.21
  Logic Optimization:                 15.75
  Mapping Optimization:               75.74
  -----------------------------------------
  Overall Compile Time:              525.16
  Overall Compile Wall Clock Time:   454.87

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
