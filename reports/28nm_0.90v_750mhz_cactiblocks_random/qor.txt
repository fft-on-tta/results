 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 18:56:34 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              33.00
  Critical Path Length:          1.23
  Critical Path Slack:           0.07
  Critical Path Clk Period:      1.33
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11232
  Leaf Cell Count:              18585
  Buf/Inv Cell Count:            3503
  Buf Cell Count:                 921
  Inv Cell Count:                2582
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16257
  Sequential Cell Count:         2328
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8634.694664
  Noncombinational Area:  6345.868721
  Buf/Inv Area:           1024.025616
  Total Buffer Area:           426.50
  Total Inverter Area:         597.53
  Macro/Black Box Area:  96178.389893
  Net Area:                  0.000000
  Net XLength        :       99625.17
  Net YLength        :       95541.25
  -----------------------------------
  Cell Area:            111158.953277
  Design Area:          111158.953277
  Net Length        :       195166.42


  Design Rules
  -----------------------------------
  Total Number of Nets:         20317
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  394.74
  Logic Optimization:                 29.85
  Mapping Optimization:              134.99
  -----------------------------------------
  Overall Compile Time:              752.29
  Overall Compile Wall Clock Time:   644.38

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
