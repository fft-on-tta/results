 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 18:56:33 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2191 (99.19%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    18 (0.81%)    |
          |                                       |                  |
          |    Total number of registers          |     2209         |
          ------------------------------------------------------------



1
