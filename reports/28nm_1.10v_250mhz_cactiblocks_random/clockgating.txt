 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 05:01:08 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |       99         |
          |                                       |                  |
          |    Number of Gated registers          |  2140 (99.21%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.79%)    |
          |                                       |                  |
          |    Total number of registers          |     2157         |
          ------------------------------------------------------------



1
