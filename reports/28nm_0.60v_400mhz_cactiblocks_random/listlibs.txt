Logical Libraries:
-------------------------------------------------------------------------
Library		File			Path
-------		----			----
  C28SOI_SC_8_CLK_LL C28SOI_SC_8_CLK_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs
  C28SOI_SC_8_CORE_LL C28SOI_SC_8_CORE_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs
  C28SOI_SC_8_COREPBP4_LL C28SOI_SC_8_COREPBP4_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs
  C28SOI_SC_8_COREPBP10_LL C28SOI_SC_8_COREPBP10_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs
  C28SOI_SC_8_COREPBP16_LL C28SOI_SC_8_COREPBP16_LL_tt28_0.60V_0.00V_0.00V_0.00V_25C.db /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs
  ram_64x51	ram_64x51.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_64x51
  ram_32x32	ram_32x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_32x32
  ram_64x32	ram_64x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_64x32
  ram_128x32	ram_128x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_128x32
  ram_256x32	ram_256x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_256x32
  ram_512x32	ram_512x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_512x32
  ram_1024x32	ram_1024x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_1024x32
  ram_2048x32	ram_2048x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_2048x32
  ram_4096x32	ram_4096x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_4096x32
  ram_8192x32	ram_8192x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_60v_8192x32
  dw_foundation.sldb dw_foundation.sldb	/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
  gtech		gtech.db		/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
  standard.sldb	standard.sldb		/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
1
