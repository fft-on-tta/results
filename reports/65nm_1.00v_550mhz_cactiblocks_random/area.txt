 
****************************************
Report : area
Design : proc
Version: M-2016.12-SP5-1
Date   : Fri Jul 27 20:49:16 2018
****************************************

Library(s) Used:

    uk65lscllmvbbl_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbl_100c25_tc.db)
    uk65lscllmvbbr_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbr_100c25_tc.db)
    uk65lscllmvbbh_100c25_tc (File: /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys/uk65lscllmvbbh_100c25_tc.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51/ram_64x51.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32/ram_32x32.db)

Number of ports:                        11473
Number of nets:                         29713
Number of cells:                        18936
Number of combinational cells:          15963
Number of sequential cells:              2742
Number of macros/black boxes:              19
Number of buf/inv:                       3318
Number of references:                      66

Combinational area:              41297.400440
Buf/Inv area:                     4906.080157
Noncombinational area:           18566.640148
Macro/Black Box area:           487756.227051
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                547620.267639
Total area:                 undefined
1
