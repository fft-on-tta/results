 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 12:38:41 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              22.00
  Critical Path Length:          1.21
  Critical Path Slack:           0.00
  Critical Path Clk Period:      1.25
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        213
  Hierarchical Port Count:      11253
  Leaf Cell Count:              19457
  Buf/Inv Cell Count:            3709
  Buf Cell Count:                 942
  Inv Cell Count:                2767
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     17082
  Sequential Cell Count:         2375
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8887.437077
  Noncombinational Area:  6480.127922
  Buf/Inv Area:           1090.284818
  Total Buffer Area:           436.40
  Total Inverter Area:         653.89
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :       91951.97
  Net YLength        :       90891.18
  -----------------------------------
  Cell Area:            111536.504452
  Design Area:          111536.504452
  Net Length        :       182843.16


  Design Rules
  -----------------------------------
  Total Number of Nets:         21135
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  359.77
  Logic Optimization:                 30.00
  Mapping Optimization:              189.57
  -----------------------------------------
  Overall Compile Time:              767.55
  Overall Compile Wall Clock Time:   625.50

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
