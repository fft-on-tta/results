 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 03:53:37 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              43.00
  Critical Path Length:          2.33
  Critical Path Slack:           0.11
  Critical Path Clk Period:      2.50
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11228
  Leaf Cell Count:              17174
  Buf/Inv Cell Count:            3116
  Buf Cell Count:                 856
  Inv Cell Count:                2260
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14871
  Sequential Cell Count:         2303
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8043.149048
  Noncombinational Area:  6272.319921
  Buf/Inv Area:            905.760014
  Total Buffer Area:           399.62
  Total Inverter Area:         506.14
  Macro/Black Box Area:  97116.966187
  Net Area:                  0.000000
  Net XLength        :       96384.24
  Net YLength        :       97530.00
  -----------------------------------
  Cell Area:            111432.435155
  Design Area:          111432.435155
  Net Length        :       193914.25


  Design Rules
  -----------------------------------
  Total Number of Nets:         18947
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  250.12
  Logic Optimization:                 21.91
  Mapping Optimization:               91.68
  -----------------------------------------
  Overall Compile Time:              556.56
  Overall Compile Wall Clock Time:   483.64

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
