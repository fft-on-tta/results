 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 02:12:03 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2203 (98.92%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    24 (1.08%)    |
          |                                       |                  |
          |    Total number of registers          |     2227         |
          ------------------------------------------------------------



1
