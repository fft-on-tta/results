 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 08:50:18 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              43.00
  Critical Path Length:          3.76
  Critical Path Slack:           0.70
  Critical Path Clk Period:      5.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        210
  Hierarchical Port Count:      11253
  Leaf Cell Count:              16688
  Buf/Inv Cell Count:            3171
  Buf Cell Count:                 849
  Inv Cell Count:                2322
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14413
  Sequential Cell Count:         2275
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7341.171479
  Noncombinational Area:  6193.222321
  Buf/Inv Area:            875.404812
  Total Buffer Area:           370.03
  Total Inverter Area:         505.38
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :       94939.05
  Net YLength        :       97165.48
  -----------------------------------
  Cell Area:            109703.333253
  Design Area:          109703.333253
  Net Length        :       192104.53


  Design Rules
  -----------------------------------
  Total Number of Nets:         18483
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  235.66
  Logic Optimization:                 11.96
  Mapping Optimization:               56.59
  -----------------------------------------
  Overall Compile Time:              505.39
  Overall Compile Wall Clock Time:   446.99

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
