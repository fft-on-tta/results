Logical Libraries:
-------------------------------------------------------------------------
Library		File			Path
-------		----			----
  uk65lscllmvbbh_100c25_tc uk65lscllmvbbh_100c25_tc.db /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBH_B03_TAPEOUTKIT/synopsys
  uk65lscllmvbbr_100c25_tc uk65lscllmvbbr_100c25_tc.db /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBR_B03_TAPEOUTKIT/synopsys
  uk65lscllmvbbl_100c25_tc uk65lscllmvbbl_100c25_tc.db /home/software/umc65nm/StdCell/G-01-LOGIC_MIXED_MODE65N-LL_LOW_K/UMK65LSCLLMVBBL_B03_TAPEOUTKIT/synopsys
  ram_64x51	ram_64x51.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x51
  ram_32x32	ram_32x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_32x32
  ram_64x32	ram_64x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_64x32
  ram_128x32	ram_128x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_128x32
  ram_256x32	ram_256x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_256x32
  ram_512x32	ram_512x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_512x32
  ram_1024x32	ram_1024x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_1024x32
  ram_2048x32	ram_2048x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_2048x32
  ram_4096x32	ram_4096x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_4096x32
  ram_8192x32	ram_8192x32.db		/home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_065nm_1_00v_8192x32
  dw_foundation.sldb dw_foundation.sldb	/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
  gtech		gtech.db		/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
  standard.sldb	standard.sldb		/home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn
1
