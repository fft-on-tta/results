 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 06:28:08 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              47.00
  Critical Path Length:          1.95
  Critical Path Slack:           0.01
  Critical Path Clk Period:      2.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11261
  Leaf Cell Count:              16537
  Buf/Inv Cell Count:            3094
  Buf Cell Count:                 874
  Inv Cell Count:                2220
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14248
  Sequential Cell Count:         2289
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7425.600273
  Noncombinational Area:  6234.675122
  Buf/Inv Area:            869.312013
  Total Buffer Area:           385.59
  Total Inverter Area:         483.72
  Macro/Black Box Area:  90431.190369
  Net Area:                  0.000000
  Net XLength        :       97602.48
  Net YLength        :       88194.02
  -----------------------------------
  Cell Area:            104091.465764
  Design Area:          104091.465764
  Net Length        :       185796.50


  Design Rules
  -----------------------------------
  Total Number of Nets:         18389
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  242.08
  Logic Optimization:                 11.20
  Mapping Optimization:               65.06
  -----------------------------------------
  Overall Compile Time:              508.39
  Overall Compile Wall Clock Time:   455.09

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
