Warning: There are 98 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 11:16:20 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         277(66.27%)       0(0.00%)          141(33.73%)        418
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         216(34.29%)       0(0.00%)          414(65.71%)        630
--------------------------------------------------------------------------------
1
