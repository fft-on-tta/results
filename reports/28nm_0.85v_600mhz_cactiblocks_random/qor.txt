 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 11:15:38 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              35.00
  Critical Path Length:          1.09
  Critical Path Slack:           0.05
  Critical Path Clk Period:      1.67
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        210
  Hierarchical Port Count:      11225
  Leaf Cell Count:              18346
  Buf/Inv Cell Count:            3421
  Buf Cell Count:                 907
  Inv Cell Count:                2514
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16022
  Sequential Cell Count:         2324
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     8526.873863
  Noncombinational Area:  6332.921520
  Buf/Inv Area:           1025.222417
  Total Buffer Area:           444.77
  Total Inverter Area:         580.45
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :      101355.17
  Net YLength        :       99944.25
  -----------------------------------
  Cell Area:            111028.734836
  Design Area:          111028.734836
  Net Length        :       201299.42


  Design Rules
  -----------------------------------
  Total Number of Nets:         20123
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  306.59
  Logic Optimization:                 25.32
  Mapping Optimization:              126.69
  -----------------------------------------
  Overall Compile Time:              648.08
  Overall Compile Wall Clock Time:   550.04

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
