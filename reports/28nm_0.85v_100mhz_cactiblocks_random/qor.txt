 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 08:15:41 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              37.00
  Critical Path Length:          3.42
  Critical Path Slack:           6.01
  Critical Path Clk Period:     10.00
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        210
  Hierarchical Port Count:      11253
  Leaf Cell Count:              16686
  Buf/Inv Cell Count:            3162
  Buf Cell Count:                 855
  Inv Cell Count:                2307
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     14411
  Sequential Cell Count:         2275
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     7297.433877
  Noncombinational Area:  6193.222321
  Buf/Inv Area:            874.860812
  Total Buffer Area:           372.75
  Total Inverter Area:         502.11
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :       91300.31
  Net YLength        :       99289.89
  -----------------------------------
  Cell Area:            109659.595651
  Design Area:          109659.595651
  Net Length        :       190590.20


  Design Rules
  -----------------------------------
  Total Number of Nets:         18451
  Nets With Violations:             2
  Max Trans Violations:             0
  Max Cap Violations:               2
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  237.43
  Logic Optimization:                 13.71
  Mapping Optimization:               57.50
  -----------------------------------------
  Overall Compile Time:              510.65
  Overall Compile Wall Clock Time:   449.92

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
