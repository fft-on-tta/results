
  Linking design 'proc'
  Using the following designs and libraries:
  --------------------------------------------------------------------------
  * (72 designs)              /home/zadnik/designs/fft_full_lbuf/proc.db, etc
  C28SOI_SC_8_CLK_LL (library) /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_0.85V_0.00V_0.00V_0.00V_25C.db
  C28SOI_SC_8_CORE_LL (library) /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CORE_LL/3.3-00/libs/C28SOI_SC_8_CORE_LL_tt28_0.85V_0.00V_0.00V_0.00V_25C.db
  C28SOI_SC_8_COREPBP4_LL (library) /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP4_LL/3.3-00/libs/C28SOI_SC_8_COREPBP4_LL_tt28_0.85V_0.00V_0.00V_0.00V_25C.db
  C28SOI_SC_8_COREPBP10_LL (library) /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP10_LL/3.3-00/libs/C28SOI_SC_8_COREPBP10_LL_tt28_0.85V_0.00V_0.00V_0.00V_25C.db
  C28SOI_SC_8_COREPBP16_LL (library) /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_0.85V_0.00V_0.00V_0.00V_25C.db
  ram_64x51 (library)         /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_64x51/ram_64x51.db
  ram_32x32 (library)         /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_32x32/ram_32x32.db
  ram_64x32 (library)         /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_64x32/ram_64x32.db
  ram_128x32 (library)        /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_128x32/ram_128x32.db
  ram_256x32 (library)        /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_256x32/ram_256x32.db
  ram_512x32 (library)        /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_512x32/ram_512x32.db
  ram_1024x32 (library)       /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_1024x32/ram_1024x32.db
  ram_2048x32 (library)       /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_2048x32/ram_2048x32.db
  ram_4096x32 (library)       /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_4096x32/ram_4096x32.db
  ram_8192x32 (library)       /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_0_85v_8192x32/ram_8192x32.db
  dw_foundation.sldb (library) /home/software/synopsys-new/installation/syn/M-2016.12-SP5-1/libraries/syn/dw_foundation.sldb

1
