 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 09:25:22 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2153 (99.22%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    17 (0.78%)    |
          |                                       |                  |
          |    Total number of registers          |     2170         |
          ------------------------------------------------------------



1
