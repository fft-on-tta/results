 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 10:29:49 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |       99         |
          |                                       |                  |
          |    Number of Gated registers          |  2186 (99.09%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    20 (0.91%)    |
          |                                       |                  |
          |    Total number of registers          |     2206         |
          ------------------------------------------------------------



1
