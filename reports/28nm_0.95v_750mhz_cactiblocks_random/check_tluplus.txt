
Sanity check for TLU+ vs MW-Tech files:
 max_tlu+: /home/software/cmos28fdsoi_25f/SynopsysBackendTechnoKit_cmos028FDSOI_6U1x_2U2x_2T8x_LB/2.0-00/TLUPLUS/FuncRCmax.tluplus
 min_tlu+: /home/software/cmos28fdsoi_25f/SynopsysBackendTechnoKit_cmos028FDSOI_6U1x_2U2x_2T8x_LB/2.0-00/TLUPLUS/FuncRCmin.tluplus
 mapping_file: /home/software/cmos28fdsoi_25f/SynopsysBackendTechnoKit_cmos028FDSOI_6U1x_2U2x_2T8x_LB/2.0-00/TLUPLUS/mapfile
 max_emul_tlu+: **NONE**
 min_emul_tlu+: **NONE**
 MW design lib: sc_mw

--------- Sanity Check on TLUPlus Files -------------
Info: Found HALF_NODE_SCALE_FACTOR 0.900000 in TLUPlus files.
1. Checking the conducting layer names in ITF and mapping file ... 
[ Passed! ]
2. Checking the via layer names in ITF and mapping file ... 
[ Passed! ]
3. Checking the consistency of Min Width and Min Spacing between MW-tech and ITF ... 
[ Passed! ]
----------------- Check Ends ------------------
1
