 
****************************************
Report : area
Design : proc
Version: M-2016.12-SP5-1
Date   : Sun Jul 29 05:18:43 2018
****************************************

Library(s) Used:

    C28SOI_SC_8_COREPBP16_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_COREPBP16_LL/3.3-00/libs/C28SOI_SC_8_COREPBP16_LL_tt28_1.10V_0.00V_0.00V_0.00V_25C.db)
    C28SOI_SC_8_CLK_LL (File: /home/software/cmos28fdsoi_25f/C28SOI_SC_8_CLK_LL/3.2-00/libs/C28SOI_SC_8_CLK_LL_tt28_1.10V_0.00V_0.00V_0.00V_25C.db)
    ram_64x51 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_64x51/ram_64x51.db)
    ram_4096x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_4096x32/ram_4096x32.db)
    ram_2048x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_2048x32/ram_2048x32.db)
    ram_1024x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_1024x32/ram_1024x32.db)
    ram_512x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_512x32/ram_512x32.db)
    ram_256x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_256x32/ram_256x32.db)
    ram_128x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_128x32/ram_128x32.db)
    ram_64x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_64x32/ram_64x32.db)
    ram_32x32 (File: /home/zadnik/designs/fft_full_lbuf/vhdl/mem/cacti_mem/ram_300k_028nm_1_10v_32x32/ram_32x32.db)

Number of ports:                        11482
Number of nets:                         28332
Number of cells:                        17370
Number of combinational cells:          14509
Number of sequential cells:              2632
Number of macros/black boxes:              19
Number of buf/inv:                       3237
Number of references:                      37

Combinational area:               7327.027478
Buf/Inv area:                      893.030412
Noncombinational area:            6193.222321
Macro/Black Box area:            90431.190369
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                103951.440168
Total area:                 undefined
1
