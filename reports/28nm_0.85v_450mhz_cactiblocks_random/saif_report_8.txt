Warning: There are 92 switching activity information conflicts. (PWR-19)
 
****************************************
Report : saif
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 10:19:20 2018
****************************************

  

--------------------------------------------------------------------------------
              User              Default           Propagated
Object type   Annotated (%)     Activity (%)      Activity (%)       Total
--------------------------------------------------------------------------------
  
 Nets         285(64.19%)       0(0.00%)          159(35.81%)        444
 Ports        229(100.00%)      0(0.00%)          0(0.00%)           229
 Pins         239(33.85%)       0(0.00%)          467(66.15%)        706
--------------------------------------------------------------------------------
1
