
Sanity check for TLU+ vs MW-Tech files:
 max_tlu+: /home/software/prostuff/scripts/65nm_rcmax.tluplus
 min_tlu+: /home/software/prostuff/scripts/65nm_rcmin.tluplus
 mapping_file: /home/software/prostuff/scripts/65nm_mapfile.tluplus
 max_emul_tlu+: **NONE**
 min_emul_tlu+: **NONE**
 MW design lib: maitoa.mw

--------- Sanity Check on TLUPlus Files -------------
1. Checking the conducting layer names in ITF and mapping file ... 
[ Passed! ]
2. Checking the via layer names in ITF and mapping file ... 
[ Passed! ]
3. Checking the consistency of Min Width and Min Spacing between MW-tech and ITF ... 
[ Passed! ]
----------------- Check Ends ------------------
1
