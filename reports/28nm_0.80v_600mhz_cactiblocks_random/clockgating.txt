 
****************************************
Report : clock_gating
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 05:10:53 2018
****************************************



                             Clock Gating Summary
          ------------------------------------------------------------
          |    Number of Clock gating elements    |      100         |
          |                                       |                  |
          |    Number of Gated registers          |  2193 (99.10%)   |
          |                                       |                  |
          |    Number of Ungated registers        |    20 (0.90%)    |
          |                                       |                  |
          |    Total number of registers          |     2213         |
          ------------------------------------------------------------



1
