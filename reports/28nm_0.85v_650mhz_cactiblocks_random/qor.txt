 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 11:35:59 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              22.00
  Critical Path Length:          0.94
  Critical Path Slack:           0.07
  Critical Path Clk Period:      1.54
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11267
  Leaf Cell Count:              19246
  Buf/Inv Cell Count:            3696
  Buf Cell Count:                 950
  Inv Cell Count:                2746
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     16916
  Sequential Cell Count:         2330
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     9139.853071
  Noncombinational Area:  6349.785520
  Buf/Inv Area:           1093.548818
  Total Buffer Area:           447.06
  Total Inverter Area:         646.49
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :      105621.20
  Net YLength        :      107289.21
  -----------------------------------
  Cell Area:            111658.578045
  Design Area:          111658.578045
  Net Length        :       212910.41


  Design Rules
  -----------------------------------
  Total Number of Nets:         20907
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  359.30
  Logic Optimization:                 27.98
  Mapping Optimization:              192.59
  -----------------------------------------
  Overall Compile Time:              771.72
  Overall Compile Wall Clock Time:   626.20

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
