 
****************************************
Report : qor
Design : proc
Version: M-2016.12-SP5-1
Date   : Sat Jul 28 11:57:11 2018
****************************************


  Timing Path Group 'clk'
  -----------------------------------
  Levels of Logic:              13.00
  Critical Path Length:          0.90
  Critical Path Slack:           0.01
  Critical Path Clk Period:      1.43
  Total Negative Slack:          0.00
  No. of Violating Paths:        0.00
  Worst Hold Violation:          0.00
  Total Hold Violation:          0.00
  No. of Hold Violations:        0.00
  -----------------------------------


  Cell Count
  -----------------------------------
  Hierarchical Cell Count:        211
  Hierarchical Port Count:      11263
  Leaf Cell Count:              19839
  Buf/Inv Cell Count:            3866
  Buf Cell Count:                1007
  Inv Cell Count:                2859
  CT Buf/Inv Cell Count:            0
  Combinational Cell Count:     17508
  Sequential Cell Count:         2331
  Macro Count:                      0
  -----------------------------------


  Area
  -----------------------------------
  Combinational Area:     9377.145885
  Noncombinational Area:  6350.438319
  Buf/Inv Area:           1158.284820
  Total Buffer Area:           472.63
  Total Inverter Area:         685.66
  Macro/Black Box Area:  96168.939453
  Net Area:                  0.000000
  Net XLength        :       93236.83
  Net YLength        :       96504.25
  -----------------------------------
  Cell Area:            111896.523658
  Design Area:          111896.523658
  Net Length        :       189741.08


  Design Rules
  -----------------------------------
  Total Number of Nets:         21417
  Nets With Violations:             0
  Max Trans Violations:             0
  Max Cap Violations:               0
  -----------------------------------


  Hostname: kaliforniantikli

  Compile CPU Statistics
  -----------------------------------------
  Resource Sharing:                  386.95
  Logic Optimization:                 30.02
  Mapping Optimization:              257.54
  -----------------------------------------
  Overall Compile Time:              866.54
  Overall Compile Wall Clock Time:   675.70

  --------------------------------------------------------------------

  Design  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0


  Design (Hold)  WNS: 0.00  TNS: 0.00  Number of Violating Paths: 0

  --------------------------------------------------------------------


1
